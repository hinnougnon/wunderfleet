# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repo is the application developed for the programming Wunder test.
The purpose of this assignment is to develop a small app which tackles the everyday problems face at Wunderfleet.

Find how it looks in folder "screenshots".

### How do I get set up? ###
* Dependencies
   - You should have mySQL 5.5.42 at least
   - You should have PHP 5.6.7 at least
   - You should have Apache server Apache/2.4.16 (why ? . because it is this version i used). 
   - You should activate "Curl" apache module if it is not already activated. It is used to call wunder amazon web service.
   Please run the php file "server_info.php" if you want to know your server details.

* Database configuration
    - Import the SQL file "manageri_wun.sql" on your mySQL. i used mySQL 5.5.42.
        You will find two tables. visitor and customer tables.
        Do not be surprised if after the first launch you find a third table (YiiSession) in the database. The third table
         is used to manage session.

* Deployment instructions
    - Put the project to the root of your web server 
    - Import SQL file on your mySQL server
    - Edit the file "db.php" to replace the parameters by your own mySQL details
        The file is returning an array as :
                           return array(
                               'connectionString' => 'mysql:host=localhost;port=8889;dbname=manageri_wun',
                               'emulatePrepare' => true,
                               'username' => 'root',
                               'password' => 'root',
                               'charset' => 'utf8',
                           );
        Normally you just need to update host,port,dbname,username,password.
        host= your mySQL host
        port = your mySQL port
        dbname= your mySQL database name
        username= your mySQL username
        password= your mySQL password
    - Make sure that the following paths are directories writable by your Web server process
        path1:  "/protected/runtime" 
        path2:  "protected/assets" 
    - To run your application , just type in your web browser : "http://your_host:your_port/app_folder/"
        Example: "http://localhost:90/wunderfleet/"
        
* Tests
    Tested with Chrome Version 69.0.3497.100 (Official Build) (64-bit)

* Logs 
    To view logs look for this folder "/protected/runtime/"
    

### Possible performance optimizations for the Code. ###
    - Use AngularJs for better structuring and optimization of javascript code.
    - Use the Ngynx server instead of Apache
    - Application logs are currently saved in a folder, but it was also possible to implement a python service that retrieves, 
        processes, and saves log files in Elastic Search. Automatic alerts can be triggered on sensitive logs to be proactive.
    - Make server-side code more modular and create more functions and reusable elements
    - Expire the form after a given time
    - Labels and messages improvements
    - More validations on variables posted on the server side
    - Writing unit tests before even coding (test driven development)
        
### Which things could be done better, than i’ve done it? ###
    - Request the customer's email on view1. 
        If the user fills the first view by providing his email, he goes to the second view and leaves the view. 
        We save when switching to the second view his email in the MySQL database. On the basis of this email, 
        we may send him (4 hours after abandoning the form) a reminder email inviting him to continue the process.
        After 3 reminders we will delete email from database.
    - Send a welcome email to the customer
    - Send the customer's bill for payment
    - Use Elastic Search to record user behavior and application logs on this page. For example, we can determine:
            * which pages or locations on a page have problems
            * the average time it takes to display the page to make the necessary optimizations, and so on
            * to know if the user is undecided to engage in order to trigger actions (online conversation)
    - Deploy the PHP application as a microservice using Docker for example.  
    - Deploy the web server (ngynx rather than apache) as a microservice using Docker for example.
    

### Who do I talk to? ###

* Repo owner or admin
Full name: Emmanuel HODONOU 
Mail: emmano3h@gmail.com 
bibucket: https://bitbucket.org/hinnougnon/