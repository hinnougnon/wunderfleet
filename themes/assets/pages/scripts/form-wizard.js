var FormWizard = function () {


    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().bootstrapWizard) {
                return;
            }

            var form = $('#submit_form');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);

            form.validate({
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    //account
                    firstname: {
                        minlength: 2,
                        maxlength: 255,
                        required: true
                    },
                    lastname: {
                        minlength: 2,
                        maxlength: 255,
                        required: true
                    },
                    telephone: {
                        required: true,
                        minlength: 10,
                        maxlength: 50,
                    },
                    address: {
                        required: true,
                        minlength: 10,
                        maxlength: 300
                    },
                    house: {
                        required: true,
                        minlength: 2,
                        maxlength: 10
                    },
                    zip: {
                        required: true,
                        minlength: 2,
                        maxlength: 10
                    },
                    city: {
                        required: true,
                        minlength: 2,
                        maxlength: 10
                    },
                    ac_owner: {
                        required: true,
                    },
                    iban: {
                        required: true,
                    },
                },

                messages: { // custom messages for radio buttons and checkboxes
                    /*'payment[]': {
                        required: "Please select at least one option",
                        minlength: jQuery.validator.format("Please select at least one option")
                    }*/
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form_gender_error");
                    } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                        error.insertAfter("#form_payment_error");
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success.hide();
                    error.show();
                    App.scrollTo(error, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                        label
                            .closest('.form-group').removeClass('has-error').addClass('has-success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label
                            .addClass('valid') // mark the current input as valid and display OK icon
                        .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    }
                },

                submitHandler: function (form) {
                    success.show();
                    error.hide();
                    form[0].submit();
                    //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
                }

            });

            var displayConfirm = function() {
                $('#tab4 .form-control-static', form).each(function(){
                    var input = $('[name="'+$(this).attr("data-display")+'"]', form);
                    if (input.is(":radio")) {
                        input = $('[name="'+$(this).attr("data-display")+'"]:checked', form);
                    }
                    if (input.is(":text") || input.is("textarea")) {
                        $(this).html(input.val());
                    } else if (input.is("select")) {
                        $(this).html(input.find('option:selected').text());
                    } else if (input.is(":radio") && input.is(":checked")) {
                        $(this).html(input.attr("data-title"));
                    } else if ($(this).attr("data-display") == 'payment[]') {
                        var payment = [];
                        $('[name="payment[]"]:checked', form).each(function(){ 
                            payment.push($(this).attr('data-title'));
                        });
                        $(this).html(payment.join("<br>"));
                    }
                });
            }

            var handleTitle = function(tab, navigation, index) {
                var total = navigation.find('li').length;
                var current = index + 1;
                // set wizard title
                $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
                // set done steps
                jQuery('li', $('#form_wizard_1')).removeClass("done");
                var li_list = navigation.find('li');
                for (var i = 0; i < index; i++) {
                    jQuery(li_list[i]).addClass("done");
                }

                if (current == 1) {
                    $('#form_wizard_1').find('.button-previous').hide();
                } else {
                    $('#form_wizard_1').find('.button-previous').show();
                }

                if (current >= total) {
                    $('#form_wizard_1').find('.button-next').hide();
                    $('#form_wizard_1').find('.button-previous').hide();
                    //$('#form_wizard_1').find('.button-submit').show();
                    displayConfirm();
                } else {
                    $('#form_wizard_1').find('.button-next').show();
                    $('#form_wizard_1').find('.button-submit').hide();
                }
                App.scrollTo($('.page-title'));
            }

            var save_form_data = function(step) {
                var form_data = form.serialize();

                jQuery.ajax({
                    "dataType":"json",
                    "type":"POST",

                    'url':'site/savedata/step/'+step,
                    "data":form_data,
                    "cache":false,
                    "success":function(answer)
                    {
                        console.log(answer);
                    },
                    "error":function(data_error)
                    {
                        console.log(data_error);
                    },

                });
            }
            var signup = function() {

            }
            // default form wizard
            $('#form_wizard_1').bootstrapWizard({
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onTabClick: function (tab, navigation, index, clickedIndex) {
                    console.log(index);
                    return false;
                    
                    success.hide();
                    error.hide();
                    if (form.valid() == false) {
                        return false;
                    }
                    
                    handleTitle(tab, navigation, clickedIndex);
                },
                onNext: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    if (form.valid() == false) {
                        return false;
                    }
                    console.log(index);
                    if(index===1 || index===2){
                        // save step 1
                        save_form_data(index);
                    }
                    if(index===3){
                        // signup
                        var form_data = form.serialize();

                        jQuery.ajax({
                            "dataType":"json",
                            "type":"POST",

                            'url':'site/signup',
                            "data":form_data,
                            "cache":false,
                            "success":function(answer)
                            {
                                console.log(answer);
                                if(answer && answer.status=='success' && answer.data){
                                    $('#payment_id').html(answer.data);
                                    $("#loading_note").addClass("hidden");
                                    $("#error_note").addClass("hidden");
                                    $("#success_note").removeClass("hidden");
                                }
                                else {
                                    $("#loading_note").addClass("hidden");
                                    $("#success_note").addClass("hidden");
                                    $("#error_note").removeClass("hidden");
                                }
                            },
                            "error":function(data_error)
                            {
                                console.log(data_error);
                                $("#loading_note").addClass("hidden");
                                $("#success_note").addClass("hidden");
                                $("#error_note").removeClass("hidden");
                                return false;
                            },

                        });
                    }

                    handleTitle(tab, navigation, index);
                },
                onPrevious: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    handleTitle(tab, navigation, index);
                },
                onTabShow: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    var $percent = (current / total) * 100;
                    $('#form_wizard_1').find('.progress-bar').css({
                        width: $percent + '%'
                    });
                }
            });

            $('#form_wizard_1').find('.button-previous').hide();
            $('#form_wizard_1 .button-submit').click(function () {
                alert('Finished! Hope you like it :)');
            }).hide();

            //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
            $('#country_list', form).change(function () {
                form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });
            console.log(visitor_step);
            if(visitor_step>1){
                for (i = 1; i <= visitor_step-1; i++) {
                    console.log("i",i);
                    $('#btn_next').click();
                }
            }


        }

    };

}();

jQuery(document).ready(function() {
    FormWizard.init();
});