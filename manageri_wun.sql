-- phpMyAdmin SQL Dump
-- version 4.4.1.1
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Oct 14, 2018 at 07:23 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `manageri_wun`
--
CREATE DATABASE IF NOT EXISTS `manageri_wun` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `manageri_wun`;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `idauto` int(11) NOT NULL COMMENT 'Id auto',
  `firstname` varchar(255) NOT NULL COMMENT 'firstname',
  `lastname` varchar(255) NOT NULL COMMENT 'lastname',
  `telephone` varchar(50) NOT NULL COMMENT 'telephone',
  `address` varchar(300) NOT NULL COMMENT 'Address including street',
  `house` varchar(10) NOT NULL COMMENT 'House number',
  `zip` varchar(10) NOT NULL COMMENT 'zip code',
  `city` varchar(100) NOT NULL COMMENT 'city',
  `paymentdataid` varchar(55) NOT NULL COMMENT 'paymentDataId',
  `creation` datetime NOT NULL COMMENT 'creation date',
  `visitorid` int(11) NOT NULL COMMENT 'visitor id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `visitor`
--

CREATE TABLE `visitor` (
  `idauto` int(11) NOT NULL COMMENT 'Id auto',
  `code` varchar(255) NOT NULL COMMENT 'vistor identification number',
  `creation` datetime NOT NULL COMMENT 'creation date'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`idauto`);

--
-- Indexes for table `visitor`
--
ALTER TABLE `visitor`
  ADD PRIMARY KEY (`idauto`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `idauto` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id auto';
--
-- AUTO_INCREMENT for table `visitor`
--
ALTER TABLE `visitor`
  MODIFY `idauto` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id auto';
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
