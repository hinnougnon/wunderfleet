<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
		'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
		'name'=>'Wunder',
		'theme' => 'wunder',
		'sourceLanguage'=>'',
		'Language'=>'en',
		'timeZone' => 'Africa/Abidjan',
	// preloading 'log' component
		'preload'=>array('log'),

	// autoloading model and component classes
		'import'=>array(
				'application.services.*',
				'application.models.*',
				'application.components.*',
		),

		'modules'=>array(
			// uncomment the following to enable the Gii tool

			/*		'gii'=>array(
                        'class'=>'system.gii.GiiModule',
                        'password'=>'y',
                        // If removed, Gii defaults to localhost only. Edit carefully to taste.
                        'ipFilters'=>array('127.0.0.1','::1'),
                    ),*/
		),
		'behaviors'=>array(
		),
	// application components
		'components'=>array(
			    // Session variables are stored in the database
				'session' => array (
						'class' => 'system.web.CDbHttpSession',
						'connectionID' => 'db',
						'sessionName' => 'Session_wunder',
				),
			// uncomment the following to enable URLs in path-format
				'urlManager'=>array(
						'showScriptName'=>false,
						'urlFormat'=>'path',
						'rules'=>array(
								'<controller:\w+>/<id:\d+>'=>'<controller>/view',
								'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
								'<controller:\w+>/<action:\w+>'=>'<controller>/<action>'
						),

				),
			/*
            'db'=>array(
                'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
            ),
            */
			// uncomment the following to use a MySQL database
				'db' => require(dirname(__FILE__) . '/../../db.php'),
				/*'db'=>array(
						'connectionString' => 'mysql:host=localhost;port=8889;dbname=manageri_wun',
						'emulatePrepare' => true,
						'username' => 'root',
						'password' => 'root',
						'charset' => 'utf8',
				),*/
				'errorHandler'=>array(
					// use 'site/error' action to display errors
						'errorAction'=>'site/error',
				),
				'log'=>array(
						'class'=>'CLogRouter',
						'routes'=>array(
								array(
										'class'=>'CFileLogRoute',
										'levels'=>'trace, info, warning',
								),
								array(
										'class'=>'CFileLogRoute',
										'logFile'=>'errors.log',
										'levels'=>'error',
								),
							// uncomment the following to show log messages on web pages

                            /*array(
                                'class'=>'CWebLogRoute',
									'enabled'=>YII_DEBUG,
                            ),*/

						),
				),
		),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
		'params'=>array(
				'adminEmail'=>'emmano3h@gmail.com',
			// emails list for site errors
				'errorsEmail' => 'emmano3h@gmail.com',
				'year'=>'2018',
				'company'=>'Wunder',
				'sitename'=>'Wunder',
		),

);