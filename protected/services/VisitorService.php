<?php

/**
 * Class VisitorService
 */
class VisitorService
{

    /**
     * Get visitor depend on code
     *
     * @param code
     * @return Visitor|null
     */
    public function getOneVisitor($code)
    {

        $criteria = new CDbCriteria;
        $criteria->compare('code', $code, false);
        return Visitor::model()->find($criteria);
    }

    /**
     * Save visitor depend on code
     *
     * @param code
     * @return Visitor|null
     */
    public function saveVisitor()
    {
        $visitor_model = new Visitor('insert');
        $visitor_code= uniqid().''.uniqid();
        $visitor_model->code = $visitor_code;
        $visitor_model->creation = date('Y-m-d H:i:s');
        try{
            $save_visitor=@$visitor_model->save();
            return $visitor_model;
        }
        catch(CDbException $e) {
            Yii::log('VisitorService saveVisitor failed to save visitor in db', $e, CLogger::LEVEL_ERROR);
            return null;
        }

    }
}
