<?php

/**
 * Class PaymentService
 */
class PaymentService
{
    /**
     * Wunder amazon service
     * @param $payment_data
     * @return mixed
     */
    public function save_payment($payment_data){
        $url = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data';
        return (new CommonService())->PostData($url,$payment_data);

    }
}
