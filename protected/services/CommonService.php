<?php

/**
 * Class CommonService
 */
class CommonService
{
    /**
     * @param $url
     * @param $values_to_post
     * @return mixed
     */
    public function PostData($url, $values_to_post)
    {
        $ch = curl_init($url);
        $timeout = 10;
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$values_to_post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($values_to_post)));
        $result =@curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}
