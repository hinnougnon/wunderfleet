<?php

/**
 * Class CustomerService
 */
class CustomerService
{
    /**
     * Save one customer
     * @param $visitorid
     * @param $firstname
     * @param $lastname
     * @param $telephone
     * @param $address
     * @param $house
     * @param $zip
     * @param $city
     * @return Customer|null
     */
    public function createCustomer($visitorid, $firstname, $lastname, $telephone, $address, $house, $zip, $city)
    {

        $customer_model = new Customer;
        $customer_model->visitorid= $visitorid;
        $customer_model->creation = date('Y-m-d H:i:s');
        $customer_model->firstname=$firstname;
        $customer_model->lastname=$lastname;
        $customer_model->telephone=$telephone;
        $customer_model->address=$address;
        $customer_model->house=$house;
        $customer_model->zip=$zip;
        $customer_model->city=$city;
        $customer_model->city=$city;
        $customer_model->paymentdataid=0;
        try{
            $save_customer=@$customer_model->save();
            return $customer_model;
        }
        catch(CDbException $e) {
            Yii::log('CustomerService createCustomer failed to save customer in db', $e, CLogger::LEVEL_ERROR);
            return null;
        }

    }

    /**
     * Remove customer depend of customer id
     * @param $id
     * @return bool
     */
    public function deleteCustomer($id)
    {
        $onecustomer = Customer::model()->findByPk($id);
        if (count($onecustomer) > 0)
        {

            try{
                return @$onecustomer->delete();
            }
            catch(CDbException $e) {
                Yii::log('CustomerService deleteCustomer failed to delete customer in db', $e, CLogger::LEVEL_ERROR);
                return false;
            }

        }
    }
}
