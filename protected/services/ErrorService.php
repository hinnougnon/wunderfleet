<?php

/**
 * Class ErrorService
 *
 */
class ErrorService
{

    const STATUS_WUNDER_API_ERROR = '1023';
    const STATUS_DB_ERROR = '1001';
    const MESSAGE_ERROR_RESPONSE = 'error';
    const MESSAGE_LOG_ERROR_RESPONSE = 'Failed to create account. Error code:';
    const MESSAGE_VISITOR_NOT_FOUND = 'Sorry but could not open your account because we did not found your session';
    const MESSAGE_ERROR_INVALID_DATA_POSTED = 'Data sent are invalid';

}
