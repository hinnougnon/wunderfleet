<?php
/**
 * This class generates response in json format for API
 *
 * @author fessnecro
 * @property int $code http code response
 * @property string $message message response
 * @property array $data data response
 */
class Response 
{
    const MSG_APIKEY_REQUIRED = 'Api key required';
    const MSG_MOBILE_REQUIRED = 'Mobile required';
    const MSG_FIELDS_EMPTY = 'FIELDS_EMPTY';
    const MSG_APIKEY_ERROR = 'APIKEY_ERROR';
     const MSG_APIKEY_OK = 'APIKEY_OK';
    const MSG_API_ERROR = 'API_ERROR';
    const MSG_TICKETS_EMPTY_LIST = 'TICKETS_EMPTY_LIST';
    const MSG_TICKET_NOT_FOUND = 'TICKET_NOT_FOUND';
    const MSG_NPS_SET_SUCCESS = 'NPS_SET_SUCCESS';
    const MSG_NPS_SET_FAILED = 'NPS_SET_FAILED';
    const NPS_INVALID='NPS_INVALID';
    const MSG_TICKET_ID_NOT_VALID='TICKET_ID_NOT_VALID';
    const MSG_NPS_ALREADY_SET='NPS_ALREADY_SET';
     const MSG_APIKEY_NOFOUND = 'Api key not found';
      const PHONE_INVALID='PHONE_INVALID';
      const TIME_ZONE_INVALID='TIME_ZONE_INVALID';
      const MSG_REQUEST_SUCCESS='REQUEST_SUCCESS';
       const MSG_REQUEST_FAILED = 'REQUEST_FAILED';
       const MSG_OFFICE_INVALID='OFFICE_INVALID';
        const MSG_SERVICE_INVALID='SERVICE_INVALID';
          const MSG_USER_INVALID='USER_INVALID';
           const MSG_SERVICE_NOT_FOUND='SERVICE_NOT_FOUND';
           const MSG_MOBILE_INVALID='MOBILE_INVALID';
            const MSG_SMS_FAILED='SMS_FAILED';
           const MSG_TICKET_GENERATED='TICKET_GENERATED';
           const MSG_TICKET_FAILED='TICKET_FAILED';
       const MSG_SERVICES_EMPTY_LIST='SERVICES_EMPTY_LIST';
       const MSG_USER_NO_GRANTED='USER_NO_GRANTED';
       const MSG_CANNOT_OPEN_SESSION='CANNOT_OPEN_SESSION';
       const MSG_USER_FOUND='USER_FOUND';
        const MSG_USER_NOT_FOUND='USER_NOT_FOUND';
	const MSG_OK = 'ok';
	const MSG_NO_DATA = 'no data';
        const MSG_FULLNAME_OR_MOBILE_REQUIRED = 'FULLNAME_OR_MOBILE_REQUIRED';
    const MSG_INTERNAL_ERROR = 'There is an error. Please try again later.';
    
    const MSG_OFFICE_REQUIRED = 'Office id cannot be blank';
    const MSG_LICENSE_REQUIRED = 'license cannot be blank';
   
    const MSG_LICENSE_NOFOUND = 'License not found or used by a office';
    
    
    const MSG_CODE_NOT_FOUND='QR_CODE_NOT_FOUND';
        const MSG_EMPTY_LIST='EMPTY_LIST';
        const MSG_DATE_REQUIRED='DATE_REQUIRED';
        const MSG_TV_ID_REQUIRED='TV_ID_REQUIRED';
        const MSG_TV_ID_NOT_FOUND='TV_ID_NOT_FOUND';
    
    
	const MSG_WRONG_INPUT = 'wrong input data';
	const MSG_UNKNOWN_ERROR = 'There is an error. Please try again later.';
	const MSG_METHOD_NOT_FOUND = 'unknown method';
	
	const MSG_IMAGE_ERROR_SIZE = 'Image is too big';
	const MSG_IMAGE_ERROR_FORMAT = 'Wrong image format';
	const MSG_IMAGE_ERROR = 'Image error';
	
	const MSG_USER_EMAIL_NOT_REGISTERED = 'This email is not registered';
	const MSG_USER_EMAIL_ALREDY_EXIST = 'This email is alread registered';
	const MSG_USER_WRONG_PASSWORD = 'USER_WRONG_PASSWORD';
	const CODE_OK = 200;
	const CODE_ERROR = 500;
	const CODE_NOT_FOUND = 404;
	
	public $data;
	
	private $code;
	private $message;
	
    public $formats=array(
			'json',
			'arraydataprovider'
			);
    public $applanguages=array(
			'en',
			'fr'
			);
   public $language='en';
   public $format='json';
	public function __construct() {
		
	}
	
	public function __set($name, $value) {
		$this->data[$name] = $value;
	}
	
	public function __isset($name) {
		return isset($this->data[$name]);
	}
	
	public function __unset($name) {
		if(isset($this->data[$name])){
			unset($this->data[$name]);
		}
	}

	public function __get($name) {
		return isset($this->data[$name]) ? $this->data[$name] : '';
	}
	
	public function __toString() {
		return json_encode($this->data);
	}
	
	

	public function setCode($code, $message){
		$this->code = $code;
		$this->message = $message;
	}
	
	public function send(){
		switch($this->code){
			case self::CODE_NOT_FOUND:
				 header('HTTP/1.0 404 Not Found');
				 break;
			case self::CODE_ERROR:
				 header('HTTP/1.0 500 Internal Error');
				 break;
			case self::CODE_OK:
				 header('HTTP/1.0 200 OK');
				 break;
		}
		
		$array = array(
		    'code' => $this->code,
		    'message' => $this->message,
		);
		
		if (isset($this->data)) {
			$array = array_merge($array, array('data' => $this->data));
		}
		
		echo json_encode($array);
	}
}
