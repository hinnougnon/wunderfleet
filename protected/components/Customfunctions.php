<?php

class Customfunctions
{
 
    /**
     * Customfunctions::SearchArrayIndex()
     * 
     * @param mixed $arraysearch  your array in format key value
     * @param mixed $column    base column of search
     * @param mixed $value           value to search
     * @return  index of column searching or nothing if nothing is being find 
     */
    public static function SearchArrayIndex($arraysearch, $column,$value)
    {
        $search = $value; // id to search

        $output = '';
        foreach ($arraysearch as $key => $array) // Look through session array
        {
            if ($array[ $column] == $search) // Found your id
            {
                $output = $key; // Set id to variable
                break; // Exit loop
            }
        }
        return $output;

    }
   
  public static function json_validate($json, $assoc_array = FALSE)
{
    // decode the JSON data
    $result = json_decode($json, $assoc_array);

    // switch and check possible JSON errors
    switch (json_last_error()) {
        case JSON_ERROR_NONE:
            $error = ''; // JSON is valid
            break;
        case JSON_ERROR_DEPTH:
            $error = 'Maximum stack depth exceeded.';
            break;
        case JSON_ERROR_STATE_MISMATCH:
            $error = 'Underflow or the modes mismatch.';
            break;
        case JSON_ERROR_CTRL_CHAR:
            $error = 'Unexpected control character found.';
            break;
        case JSON_ERROR_SYNTAX:
            $error = 'Syntax error, malformed JSON.';
            break;
        // only PHP 5.3+
        case JSON_ERROR_UTF8:
            $error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
            break;
        default:
            $error = 'Unknown JSON error occured.';
            break;
    }

    if($error !== '') {
        // throw the Exception or exit
       return false;
    }

    // everything is OK
    return true;
}

}

?>