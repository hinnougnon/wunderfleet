<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
      private $_id;
    public $Error;
    public function authenticate()
    {
$criteria = new CDbCriteria();

            $criteria->condition = 'Username=:Username';
            $criteria->params = array(':Username' => $this->username);
            $user = User::model()->find($criteria);
            if (count($user) == 0)
            {
                //  No user found!
            $this->errorCode = self::ERROR_USERNAME_INVALID;
          //  $this->Error="user not find";
            }
           
        else
            if ($user->Password !== hash_hmac('sha256', $this->password, Yii::app()->params['encryptionKey']))
            {
                // Invalid password!
                $this->errorCode = self::ERROR_PASSWORD_INVALID;
               // $this->Error="password invalid".$user->IdAuto;
            }
            else
            {
               // Okay!
                $this->errorCode = self::ERROR_NONE;
                $this->setState('IdAuto', $user->IdAuto);
                $this->_id = $user->IdAuto;
                $this->setState('Username', $user->Username);

                $this->setState('Password', $user->Password);
                 $this->setState('Firstname', $user->Firstname);
              $this->setState('Lastname', $user->Lastname);
              $this->setState('ProfileId', $user->IdProfile);
              
          
               // $this->Error="state set".$user->IdAuto;

            }
            return !$this->errorCode;


    }
     public function getId()
    {
        return $this->_id;
    }
}