<?php

class SiteController extends Controller
{

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        $this->layout = 'ErrorLayout';
        if ($error = Yii::app()->errorHandler->error)
        {


            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Action called to show the signup page
     */
    public function actionIndex()
    {
        $this->layout='customerLayout';
        // check user session
        if (isset(Yii::app()->session['visitor_code']) && !empty(Yii::app()->session['visitor_code'])){
            // not the user first visit, request to get user data already saved
            $visitor_code= trim(Yii::app()->session['visitor_code']);
            $visitor_model= (new VisitorService)->getOneVisitor($visitor_code);
            if(!empty($visitor_model)){
                $this->render('index', array('visitormodel'=>$visitor_model));
            }
            else{
                // visitor not found in db,
                // user first visit, let's open a session
                $this->save_visitor();
            }

        }
        else{
            // user first visit, let's open a session
            $this->save_visitor();

        }

    }

    /**
     * We identify user who open the page.
     */
    private function save_visitor(){
        try{
            $objvisitor=@(new VisitorService)->saveVisitor();
            if($objvisitor && $objvisitor!=null){
                // data saved, let's set session data now
                Yii::app()->session['visitor_code']= $objvisitor->code;
                $this->render('index', array('visitormodel'=>$objvisitor));
            }
            else{
                // data not saved, let's show error page
                Yii::log('SiteController save_visitor $objvisitor null', ErrorService::STATUS_DB_ERROR, CLogger::LEVEL_ERROR);
                $this->render('error');
            }
        }
        catch(Exception $e) {
            Yii::log('SiteController save_visitor $objvisitor catch to save in db', $e, CLogger::LEVEL_ERROR);
            $this->render('error');
        }
    }

    /**
     * SAve data locally on the two form steps
     * @param $step
     */
    public function actionsavedata($step)
    {
        if (!isset($_POST))
        {
            echo json_encode(array(
                'status' => ErrorService::MESSAGE_ERROR_RESPONSE,
                'error' => ErrorService::MESSAGE_ERROR_INVALID_DATA_POSTED,
            ));
            Yii::app()->end();
        }

        if($step == 1){
            Yii::app()->session['firstname'] = CHtml::encode($_POST['firstname']);
            Yii::app()->session['lastname'] = CHtml::encode($_POST['lastname']);
            Yii::app()->session['telephone'] = CHtml::encode($_POST['telephone']);
            Yii::app()->session['form_step']=$step+1;
        }
        if($step == 2){
            Yii::app()->session['address'] = CHtml::encode($_POST['address']);
            Yii::app()->session['house'] = CHtml::encode($_POST['house']);
            Yii::app()->session['zip'] = CHtml::encode($_POST['zip']);
            Yii::app()->session['city'] = CHtml::encode($_POST['city']);
            Yii::app()->session['form_step']=$step+1;
        }
        echo json_encode(array(
            'status' => SuccessService::MESSAGE_SUCCESS_RESPONSE
        ));
        Yii::app()->end();

    }

    /**
     * To signup
     */
    public function actionsignup()
    {
        // check visitor code as csrf
        if (!isset(Yii::app()->session['visitor_code'])){
            echo json_encode(array(
                'status' => ErrorService::MESSAGE_ERROR_RESPONSE,
                'error' => ErrorService::MESSAGE_VISITOR_NOT_FOUND,
            ));
            Yii::app()->end();
        }
        if (empty(Yii::app()->session['visitor_code'])){
            echo json_encode(array(
                'status' => ErrorService::MESSAGE_ERROR_RESPONSE,
                'error' => ErrorService::MESSAGE_VISITOR_NOT_FOUND,
            ));
            Yii::app()->end();
        }
        if (!$this->validate_signupdata($_POST)){
            echo json_encode(array(
                'status' => ErrorService::MESSAGE_ERROR_RESPONSE,
                'error' => ErrorService::MESSAGE_ERROR_INVALID_DATA_POSTED,
            ));
            Yii::app()->end();
        }

        // check visitor session in db
        $visitor_code= trim(Yii::app()->session['visitor_code']);
        $visitor_model= (new VisitorService)->getOneVisitor($visitor_code);
        if(empty( $visitor_model)){
            echo json_encode(array(
                'status' => ErrorService::MESSAGE_ERROR_RESPONSE,
                'error' => ErrorService::MESSAGE_VISITOR_NOT_FOUND,
            ));
            Yii::app()->end();
        }
        // save user data
        // save model in db
        $customer_model=@(new CustomerService())->createCustomer($visitor_model->idauto,trim($_POST['firstname']),trim($_POST['lastname']),trim($_POST['telephone']),trim($_POST['address']),trim($_POST['house']),trim($_POST['zip']),trim($_POST['city']));
        if ($customer_model && !empty($customer_model)){
            // let's save data in wunder amazon ws
            $payment_data = json_encode(array(
                "customerId"=>$customer_model->idauto,
                "iban"=>$_POST['iban'],
                "owner"=>(isset($_POST['ac_owner']) && !empty($_POST['ac_owner']))?trim($_POST['ac_owner']):'none'
            ));
            $result_data_payment =(new PaymentService())->save_payment($payment_data);
            if ((isset($result_data_payment)) && $result_data_payment != null && Customfunctions::
                json_validate($result_data_payment))
            {
                $result_data_payment = json_decode($result_data_payment, true);
                if (isset($result_data_payment) && is_array($result_data_payment) && $result_data_payment != null &&
                    isset($result_data_payment['paymentDataId'])){
                    // data saved
                    // save payment data id
                    $customer_model->paymentdataid = $result_data_payment['paymentDataId'];
                    $save_customer_model=@$customer_model->update(false);
                    // let's clear variables in session
                    Yii::app()->session->clear();
                    // send response
                    echo json_encode(array(
                        'status' => SuccessService::MESSAGE_SUCCESS_RESPONSE,
                        'message' => 'New account created',
                        'data' => $result_data_payment['paymentDataId'],
                    ));

                }else{
                    // data failed to save on amazon ws
                    // log data
                    Yii::log('SiteController actionsignup Failed to create account. After decoding data came from server, data was not valid.'.var_export($payment_data,true), CLogger::LEVEL_ERROR);

                    // cancel customer datasaved
                    $remove_customer = @(new CustomerService())->deleteCustomer($customer_model->idauto);
                    // send response
                    echo json_encode(array(
                        'status' => ErrorService::MESSAGE_ERROR_RESPONSE,
                        'error' => ErrorService::MESSAGE_LOG_ERROR_RESPONSE.ErrorService::STATUS_WUNDER_API_ERROR,
                    ));
                }
            }
            else{
                // data failed to save on amazon ws
                // log data
                Yii::log('SiteController actionsignup Failed to create account. data failed to save on amazon ws. Invalid response received from server '.var_export($payment_data,true), CLogger::LEVEL_ERROR);
                // cancel customer datasaved
                $remove_customer = @(new CustomerService())->deleteCustomer($customer_model->idauto);
                // send response
                echo json_encode(array(
                    'status' => ErrorService::MESSAGE_ERROR_RESPONSE,
                    'error' => ErrorService::MESSAGE_LOG_ERROR_RESPONSE.ErrorService::STATUS_WUNDER_API_ERROR,
                ));
            }


        }
        else{
            // log data
            Yii::log('SiteController actionsignup Failed to create account . Error comes when start to save data in customer table. code:'.var_export($_POST,true), CLogger::LEVEL_ERROR);
            // send response
            echo json_encode(array(
                'status' => ErrorService::MESSAGE_ERROR_RESPONSE,
                'error' => ErrorService::MESSAGE_LOG_ERROR_RESPONSE.ErrorService::STATUS_DB_ERROR,
            ));
        }


    }

    /**
     * To check data posted from the frontend
     * @param $_dataposted
     * @return bool
     */
    private function validate_signupdata($_dataposted){
        if (!isset($_dataposted) ){
            return false;
        }
        if (!isset($_dataposted['firstname']) || !isset($_dataposted['lastname']) ||!isset($_dataposted['telephone']) ||!isset($_dataposted['address']) ||!isset($_dataposted['house']) ||!isset($_dataposted['zip']) ||!isset($_dataposted['city']) || !isset($_dataposted['iban'])){
            return false;
        }
        if (empty($_dataposted['firstname']) || empty($_dataposted['lastname']) || empty($_dataposted['telephone']) ||empty($_dataposted['address']) ||empty($_dataposted['house']) ||empty($_dataposted['zip']) ||empty($_dataposted['city']) ||empty($_dataposted['iban'])){
            return false;
        }
        return true;
    }

}