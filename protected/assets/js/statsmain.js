  jQuery(document).ready(function() {    
   
  var StatsController  = function () {

 exportoverviewtickets = function(){
	         window.location=(baseUrl+'/dashboard/stats/exportoverviewtickets');
			
                
				return false;
			}
            exportofficetickets = function(){
	         window.location=(baseUrl+'/dashboard/stats/exportofficetickets');
			
                
				return false;
			}
            exportusertickets = function(){
	         window.location=(baseUrl+'/dashboard/stats/exportusertickets');
			
                
				return false;
			}
	statistikcurrentoffice = function(p1,p2,officeid){
		
                 jQuery.ajax({
                 'dataType':'html',
                 'type':'get',
                 'url':baseUrl+'/dashboard/stats/displaycurrentoffice/date1/'+p1+'/date2/'+p2+'/officeId/'+officeid,
                 'cache':false,
                 'beforeSend':function( request )
                     {
                      $('#statistiq_result').html("<img src="+baseUrl+'/images/728.GIF'+">");
                     },
                 'success':function(data)
                  {
                        $('#statistiq_result').html(data);         
                  },
                  'error':function(data) 
                  {
                        
                  alert(_msgproblemconnect);
                                            
                  },
                                      
                 });
				return false;
			}
          
       
 ///////////////////   Overview Charts

$('#headertab_o_1').click( function() {
    _currenttab="tab_o_1";
     getoverviewcharts(_chartstart,_chartend); 
});
$('#headertab_o_2').click( function() {
    _currenttab="tab_o_2";
     getoverviewcharts(_chartstart,_chartend); 
});
$('#headertab_o_3').click( function() {
    _currenttab="tab_o_3";
     getoverviewcharts(_chartstart,_chartend); 
});
 getoverviewcharts= function(p1,p2){  
   if(typeof _currenttab === 'undefined' )
   {
     _currenttab="tab_o_1"; 
   }
     switch(_currenttab) {
    case "tab_o_1":
   donutpercentnbreticketsbyoffice(p1,p2);
   donutpercentfinishnoshow(p1,p2);
   donutoverviewpercentnbreticketsbyservice(p1,p2);
   request_donutoverviewchannelservednoshow(p1,p2);
     break
    case "tab_o_2":
   averageminutesalloffice(p1,p2);
   averageminutesallservice(p1,p2);
     break
     case "tab_o_3":
   overview_barnbreticketsbyservice(p1,p2);
   customerstotalreceivenoshowalloffice(p1,p2);
   request_baroverviewchannelservednoshow(p1,p2);
     break
    default:
     donutpercentnbreticketsbyoffice(p1,p2);
   donutpercentfinishnoshow(p1,p2);
   donutoverviewpercentnbreticketsbyservice(p1,p2);
   request_donutoverviewchannelservednoshow(p1,p2); 
     break
}
          
    return false;
  }
            display_donutnbreticketsbyoffice = function(mydata){ 
                 
              $('#tab_o_1_chart1').highcharts({
        chart: {
           plotBackgroundColor: null,
            plotBorderWidth: 1,//null,
            plotShadow: false
        },
        title: {
            text: _titlechart1,
            useHTML: (_ISRTL == 1) ? Highcharts.hasBidibug : false
        },
        tooltip: {
            pointFormat: '{point.label} : <b>{point.percentage:.2f}%</b>',
            useHTML : (_ISRTL == 1) ? true : false
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.label}</b>: {point.percentage:.2f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    },
                    useHTML: (_ISRTL == 1) ? true : false,
                },
                    
            }
        },
        
        
        credits: {
            enabled: false
        },
         series: [
         {
         type: 'pie',
           name: 'kkk',
            
            data:mydata,
            
         }
        
        ]
    });
    return false;
				
			}
     
         donutpercentnbreticketsbyoffice= function(p1,p2){ 
             jQuery.ajax({
                 'dataType':'json',
                 'type':'get',
                 'url':baseUrl+'/dashboard/stats/percentticketsbyoffice/date1/'+p1+'/date2/'+p2,
                 'cache':false,
                 'beforeSend':function( request )
                     {
                      $('#tab_o_1_chart1').html(_msgbusy);
                     },
                 'success':function(data)
                  {
                    var myserie=$.parseJSON(data);
                    
                   
                     display_donutnbreticketsbyoffice(myserie);
                      
                  },
                  'error':function(data) 
                  {
                        
                       alert(_msgproblemconnect);                   
                  },
                                      
                 });                      
        return false;     
    }
    display_overviewdonutpercentnbreticketsbyservice = function(mydata){ 
                 
              $('#tab_o_1_chart2').highcharts({
        chart: {
           plotBackgroundColor: null,
            plotBorderWidth: 1,//null,
            plotShadow: false
        },
        title: {
            text: _titlechart1_2,
            useHTML: (_ISRTL == 1) ? Highcharts.hasBidibug : false
        },
        tooltip: {
            pointFormat: '{point.label}: <b>{point.percentage:.2f}%</b>',
            useHTML: (_ISRTL == 1) ? true : false
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.label}</b>: {point.percentage:.2f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    },
                    useHTML : (_ISRTL == 1) ? true : false
                }
            }
        },
        
        
        credits: {
            enabled: false
        },
         series: [
         {
         type: 'pie',
           name: '',
            
            data:mydata,
            
         }
        
        ]
    });
    return false;
				
			}
    donutoverviewpercentnbreticketsbyservice= function(p1,p2){ 
            
             jQuery.ajax({
                 'dataType':'json',
                 'type':'get',
                'url':baseUrl+'/dashboard/stats/overviewpercentticketsbysvc/date1/'+p1+'/date2/'+p2,
                 'cache':false,
                 'beforeSend':function( request )
                     {
                      $('#tab_o_1_chart2').html(_msgbusy);
                     },
                 'success':function(data)
                  {
                    var myserie=$.parseJSON(data);
                    
                   
                    if(myserie[0].y==0)
                    {
                        $('#tab_o_1_chart2').html(_msgnodata);
                    }
                     else{
                     display_overviewdonutpercentnbreticketsbyservice(myserie);
                  }                
        
                  },
                  'error':function(data) 
                  {
                        
                       alert(_msgproblemconnect);                   
                  },
                                      
                 });            
        return false;     
    }
    display_donutpercentfinishnoshow = function(mydata){ 
                 
              $('#tab_o_1_chart3').highcharts({
        chart: {
           plotBackgroundColor: null,
            plotBorderWidth: 1,//null,
            plotShadow: false
        },
        title: {
            text: _titlechart2,
            useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
        },
        tooltip: {
            pointFormat: '{point.label}: <b>{point.percentage:.2f}%</b>',
            useHTML: (_ISRTL == 1) ? true : false
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.label}</b>: {point.percentage:.2f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    },
                    useHTML: (_ISRTL == 1) ? true : false
                }
            }
        },
        
        
        credits: {
            enabled: false
        },
         series: [
         {
         type: 'pie',
           name: '',
            
            data:mydata,
            
         }
        
        ]
    });
    return false;
				
			}
    donutpercentfinishnoshow= function(p1,p2){ 
            
             jQuery.ajax({
                 'dataType':'json',
                 'type':'get',
                 'url':baseUrl+'/dashboard/stats/percentticketsServiceNoshow/date1/'+p1+'/date2/'+p2,
                 'cache':false,
                 'beforeSend':function( request )
                     {
                        $('#tab_o_1_chart3').html(_msgbusy);
                     },
                 'success':function(data)
                  {
                    var myserie=$.parseJSON(data);                 
                    if(myserie[0].y==0)
                    {
                        $('#tab_o_1_chart3').html(_msgnodata);
                    }
                     else{
                     display_donutpercentfinishnoshow(myserie);
                  }                 
        
                  },
                  'error':function(data) 
                  {
                        
                       alert(_msgproblemconnect);                   
                  },
                                      
                 });         
        return false;    
    }
            
    display_donutoverviewchannelservednoshow = function(mydata){ 
                 
              $('#tab_o_1_chart4').highcharts({
        chart: {
           plotBackgroundColor: null,
            plotBorderWidth: 1,//null,
            plotShadow: false
        },
        title: {
            text: _titlechart1_4,
            useHTML: (_ISRTL == 1) ? Highcharts.hasBidibug : false
        },
        tooltip: {
            pointFormat: '{point.label}: <b>{point.percentage:.2f}%</b>',
            useHTML : (_ISRTL == 1) ? true : false
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.label}</b>: {point.percentage:.2f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    },
                    useHTML : (_ISRTL == 1) ? true : false
                }
            }
        },
        
        
        credits: {
            enabled: false
        },
         series: [
         {
         type: 'pie',
           name: '',
            
            data:mydata,
            
         }
        
        ]
    });
    return false;
				
			}
    request_donutoverviewchannelservednoshow= function(p1,p2){ 
            
             jQuery.ajax({
                 'dataType':'json',
                 'type':'get',
                 'url':baseUrl+'/dashboard/stats/OverviewDonutRemoteAndOnsiteChannel/date1/'+p1+'/date2/'+p2,
                 'cache':false,
                 'beforeSend':function( request )
                     {
                        $('#tab_o_1_chart4').html(_msgbusy);
                     },
                 'success':function(data)
                  {
                    var myserie=$.parseJSON(data);
                    
                   
                    if(myserie[0].y==0)
                    {
                        $('#tab_o_1_chart4').html(_msgnodata);
                    }
                     else{
                     display_donutoverviewchannelservednoshow(myserie);
                      }  
                  },
                  'error':function(data) 
                  {
                        
                       alert(_msgproblemconnect);                   
                  },
                                      
                 });         
        return false;     
    }         
            
            
    display_averageminutesbyoffice = function(myseries1,myseries2){ 
                 
              $('#tab_o_2_chart1').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: _titlechart3,
            useHTML: (_ISRTL == 1) ? Highcharts.hasBidibug : false
        },
        subtitle: {
            text: ''
        },
       
        xAxis: {
            //reversed: true,
          categories: [],
          labels: {
            useHTML: (_ISRTL == 1) ? Highcharts.hasBidibug : false,
            x : (_ISRTL == 1) ? 100 : -5
          },
          
            
            opposite: (_ISRTL == 1) ? true : false

        },
        yAxis: {
            min: 0,
            title: {
                text: _labeltime+' '+_Label_graph_minute_1,
                align: 'high',
                useHTML: (_ISRTL == 1) ? true : false
            },
            labels: {
                overflow: 'justify'
            },

            opposite: false,
            reversed: (_ISRTL == 1) ? true : false
        },
        tooltip: {
            valueSuffix: ' '+_Label_graph_min_1,
            useHTML: (_ISRTL == 1) ? true : false
        },
        plotOptions: {
            bar: {
                 dataLabels: {
                    enabled: true,
                    formatter: function () {
                        return '' + this.point.text+_Label_graph_min_1
                    },
                    useHTML: (_ISRTL == 1) ? true : false
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: (_ISRTL == 1) ? 'left' : 'right',
            verticalAlign: 'top',
            x: (_ISRTL == 1) ? 40 : -40,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true,
            useHTML: (_ISRTL == 1) ? true : false
        },
        credits: {
            enabled: false
        },
         series: [
         {
             name: _labelwaiting,
            
            data:myseries1,
        },
        
         {
             name: _labelservice,
             // color:rgb(169, 255, 150),
            data: myseries2,
        }
        
        
        ]
    });
    return false;
				
			}
               
averageminutesalloffice= function(p1,p2){ 
            
             jQuery.ajax({
                 'dataType':'json',
                 'type':'get',
                 'url':baseUrl+'/dashboard/stats/AveragetimeAllOffice/date1/'+p1+'/date2/'+p2,
                 'cache':false,
                 'beforeSend':function( request )
                     {
                        $('#tab_o_2_chart1').html(_msgbusy);
                     },
                 'success':function(data)
                  {
                    
                       var series1=$.parseJSON(data.series1);
                      var series2 =$.parseJSON(data.series2);
                 
                  if(series1[0].y==0)
                    {
                        $('#tab_o_2_chart1').html(_msgnodata);
                    }
                    else{
                    display_averageminutesbyoffice(series1,series2);
                  }
        
                  },
                  'error':function(data) 
                  {
                        
                       alert(_msgproblemconnect);                   
                  },
                                      
                 });
            
            
        return false;
            
              
       
    }

display_averageminutesbyservice = function(myseries1,myseries2){ 
                 
              $('#tab_o_2_chart2').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: _titlechart_new,
            useHTML: (_ISRTL == 1 )? Highcharts.hasBidibug : false
        },
        subtitle: {
            text: ''
        },
       
        xAxis: {
          categories: [],
          labels: {
            useHTML: (_ISRTL == 1) ? Highcharts.hasBidibug : false,
            x : (_ISRTL == 1) ? 100 : -5
          },
            opposite: (_ISRTL == 1) ? true : false
            //title: {
//                text: null
//            }
        },
        yAxis: {
            min: 0,
            title: {
                text: _labeltime+' '+_Label_graph_minute_1,
                align: 'high',
                useHTML: (_ISRTL == 1) ? true : false
            },
            labels: {
                overflow: 'justify'
            },
            
            reversed: (_ISRTL == 1) ? true : false
        },
        tooltip: {
            valueSuffix: ' '+_Label_graph_min_1,
            useHTML: (_ISRTL == 1) ? true : false
        },
        plotOptions: {
            bar: {
                 dataLabels: {
                    enabled: true,
                    formatter: function () {
                        return '' + this.point.text+_Label_graph_min_1
                    },
                    useHTML: (_ISRTL == 1) ? true : false
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: (_ISRTL == 1) ? 'left' : 'right',
            verticalAlign: 'top',
            x: (_ISRTL == 1) ? 40 : -40,
            y: 25,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true,
            useHTML: (_ISRTL == 1) ? true : false
        },
        credits: {
            enabled: false
        },
         series: [
         {
             name: _labelwaiting,
            
            data:myseries1,
        },
        
         {
             name: _labelservice,
             // color:rgb(169, 255, 150),
            data: myseries2,
        }
        
        
        ]
    });
    return false;
                
            }


averageminutesallservice= function(p1,p2){ 
            
             jQuery.ajax({
                 'dataType':'json',
                 'type':'get',
                 'url':baseUrl+'/dashboard/stats/AveragetimeAllService/date1/'+p1+'/date2/'+p2,
                 'cache':false,
                 'beforeSend':function( request )
                     {
                        $('#tab_o_2_chart2').html(_msgbusy);
                     },
                 'success':function(data)
                  {
                    
                       var series1=$.parseJSON(data.series1);
                      var series2 =$.parseJSON(data.series2);
                 
                  if(series1[0].y==0)
                    {
                        $('#tab_o_2_chart2').html(_msgnodata);
                    }
                    else{
                        //alert(series1);
                    display_averageminutesbyservice(series1,series2);
                  }
        
                  },
                  'error':function(xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                        
                        alert(_msgproblemconnect);                   
                  },
                                      
                 });
            
            
        return false;
            
              
       
    }



       display_overview_barnbreticketsbyservice = function(mydata){ 
                 
              $('#tab_o_3_chart1').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: _titlechart5,
            useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
        },
        subtitle: {
            text: ''
        },
       
        xAxis: {
          categories: [],
            title: {
             text: _Label_graph_services_1,
             x: (_ISRTL == 1) ? -50 : -10,
             useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
          },
          labels: {
            useHTML: (_ISRTL == 1) ? Highcharts.hasBidibug : false,
            x : (_ISRTL == 1) ? 100 : -5
          },
          opposite: (_ISRTL == 1) ? true : false
        },
        yAxis: {
            min: 0,
            title: {
                text: _Label_graph_tickets_1,
                align: 'high',
                useHTML : (_ISRTL == 1) ? true : false
            },
            labels: {
                overflow: 'justify'
            },
            opposite: false,
            reversed: (_ISRTL == 1) ? true : false
        },
        tooltip: {
            valueSuffix: ' ',
            useHTML : (_ISRTL == 1) ? true : false
        },
        plotOptions: {
            bar: {
                 dataLabels: {
                    enabled: true,
                    formatter: function () {
                        return '' + this.point.text
                    },
                    useHTML: (_ISRTL == 1) ? true : false
                }
            }
        },
        
        credits: {
            enabled: false
        },
        legend: {
            layout: 'vertical',
            align: (_ISRTL == 1) ? 'left' : 'right',
            verticalAlign: 'top',
            x: (_ISRTL == 1) ? 20 : -20,
            y: 50,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true,
            useHTML: (_ISRTL == 1) ? true : false
        },
         series: [
         {
            name: _Label_graph_tickets_1,
            //useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false,
            data:mydata,

        }
        
        
        
        
        ]
    });
    return false;
				
			}
               
         overview_barnbreticketsbyservice= function(p1,p2,officeid){ 
            
             jQuery.ajax({
                 'dataType':'json',
                 'type':'get',
                  'url':baseUrl+'/dashboard/stats/displayoverviewsvcbytickets/date1/'+p1+'/date2/'+p2,
                 'cache':false,
                 'beforeSend':function( request )
                     {
                      $('#tab_o_3_chart1').html(_msgbusy);
                       $('#summary_tab_o_3_chart1').html("");
                     },
                 'success':function(data)
                  {
                    
                     var myserie=$.parseJSON(data.datas);
                   $('#summary_tab_o_3_chart1').html(data.summary);
                  if(myserie[0].y==0)
                    {
                        $('#tab_o_3_chart1').html(_msgnodata);
                    }
                    else{
                    display_overview_barnbreticketsbyservice(myserie);
                  }
        
                  },
                  'error':function(data) 
                  {
                        
                       alert(_msgproblemconnect);                   
                  },
                                      
                 });
            
            
        return false;
            
              
       
    }      
     
      
      display_customerstotalreceivenoshowalloffice = function(myseries1,myseries2,myseries3){ 
                 
              $('#tab_o_3_chart2').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: _titlechart6,
            useHTML: (_ISRTL == 1) ? Highcharts.hasBidibug : false
        },
        subtitle: {
            text: ''
        },
       
        xAxis: {
          categories: [],
          reversed : (_ISRTL == 1) ? true : false,
          opposite : (_ISRTL == 1) ? true : false,
          labels: {
            useHTML: (_ISRTL == 1) ? Highcharts.hasBidibug : false,
            x : (_ISRTL == 1) ? 100 : -5
          },
            //title: {
//                text: null
//            }
        },
        yAxis: {
            min: 0,
            title: {
                text: _Label_graph_tickets_1,
                align: 'high',
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            },
            labels: {
                overflow: 'justify'
            },
            reversed: (_ISRTL == 1) ? true : false
        },
        tooltip: {
           formatter: function () {
                return '<b>' + this.point.name + '</b><br/>' +
                    this.series.name + ': ' + this.y + ' '+_Label_graph_tickets_1+'<br/>' +
                    _Label_graph_total_1+': ' + this.point.stackTotal+' '+_Label_graph_tickets_1;
            },
            useHTML: (_ISRTL == 1) ? true : false
        },
        plotOptions: {
            
            series: {
                stacking: 'normal'
            },
            bar: {
                 dataLabels: {
                    enabled: true,
                    formatter: function () {
                        return '' + this.point.text
                    },
                    useHTML : (_ISRTL == 1) ? true : false
                }
            }
            
            
        },
        legend: {
            layout: 'vertical',
            align: (_ISRTL == 1) ? 'left' : 'right',
            verticalAlign: 'top',
            x: (_ISRTL == 1) ? 40 : -40,
            y: 50,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true,
            useHTML : (_ISRTL == 1) ? true : false
        },
        credits: {
            enabled: false
        },
         series: [
         
        
         
        {
             name: _labelnoshow,
             color:'rgb(67, 67, 72)',
            data: myseries3,
        },
         {
             name: _labelserve,
              color:'rgb(124, 181, 236)',
            data: myseries2,
        },
        
        
        
        ]
    });
    return false;
				
			}
               
         customerstotalreceivenoshowalloffice= function(p1,p2){ 
            
             jQuery.ajax({
                 'dataType':'json',
                 'type':'get',
                 'url':baseUrl+'/dashboard/stats/CustomersTotalReceiveNoshowAllOffice/date1/'+p1+'/date2/'+p2,
                 'cache':false,
                 'beforeSend':function( request )
                     {
                        $('#tab_o_3_chart2').html(_msgbusy);
                     },
                 'success':function(data)
                  {
                    
                       var series1=$.parseJSON(data.series1);
                      var series2 =$.parseJSON(data.series2);
                 var series3 =$.parseJSON(data.series3);
                  if(series1[0].y==0)
                    {
                        $('#tab_o_3_chart2').html(_msgnodata);
                    }
                    else{
                    display_customerstotalreceivenoshowalloffice(series1,series2,series3);
                  }
        
                  },
                  'error':function(data) 
                  {
                        
                       alert(_msgproblemconnect);                   
                  },
                                      
                 });
            
            
        return false;
            
              
       
    }
    
    
     
   
      
        display_baroverviewchannelservednoshow = function(myseries1,myseries2){ 
                 
              $('#tab_o_3_chart3').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: _titlechart1_4,
            useHTML: (_ISRTL == 1) ? Highcharts.hasBidibug : false
        },
        subtitle: {
            text: '',

        },
       
        xAxis: {
          categories: [],
          opposite : (_ISRTL == 1) ? true : false,
          reversed : (_ISRTL == 1) ? true : false,
          useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false,
          labels: {
            useHTML: (_ISRTL == 1) ? Highcharts.hasBidibug : false,
            x : (_ISRTL == 1) ? 100 : -5
          },
            //title: {
//                text: null
//            }

        },
        yAxis: {
            min: 0,
            title: {
                text: _Label_graph_tickets_1,
                align: 'high',
                useHTML: (_ISRTL == 1) ? Highcharts.hasBidibug : false
            },
            labels: {
                overflow: 'justify'
            },
            reversed: (_ISRTL == 1) ? true : false
            
        },
        tooltip: {
           // valueSuffix: ' min'
           formatter: function () {
                return '<b>' + this.point.name + '</b><br/>' +
                    this.series.name + ': ' + this.y + ' '+_Label_graph_tickets_1+'<br/>' +
                    _Label_graph_total_1+': ' + this.point.stackTotal+' '+_Label_graph_tickets_1;
            },
            useHTML: (_ISRTL == 1) ? true : false
        },
      
         plotOptions: {
            
            series: {
                stacking: 'normal',
            },
            bar: {
                 dataLabels: {
                    enabled: true,
                    formatter: function () {
                        return '' + this.point.text+' '
                    },
                    useHTML: (_ISRTL == 1) ? true : false
                }
            }
            
            
        },
        legend: {
            layout: 'vertical',
            align: (_ISRTL == 1) ? 'left' : 'right',
            verticalAlign: 'top',
            x: (_ISRTL == 1) ? 20 : -20,
            y: 25,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true,
            useHTML : (_ISRTL == 1) ? true : false
        },
      
        credits: {
            enabled: false
        },
         series: [
         
        
         {
             name: _labelnoshow,
             color:'rgb(67, 67, 72)',
            data: myseries2,
        }
        ,{
             name:_labelserve ,
            color:' rgb(124, 181, 236)',
            data:myseries1,
        }
        
        ]
    });
    return false;
				
			}
               
         request_baroverviewchannelservednoshow= function(p1,p2){ 
            
             jQuery.ajax({
                 'dataType':'json',
                 'type':'get',
                 'url':baseUrl+'/dashboard/stats/OverviewBarRemoteAndOnsiteChannel/date1/'+p1+'/date2/'+p2,
                 'cache':false,
                 'beforeSend':function( request )
                     {
                      $('#tab_o_3_chart3').html(_msgbusy);
                     },
                 'success':function(data)
                  {
                   
                       var series1=$.parseJSON(data.series1);
                      var series2 =$.parseJSON(data.series2);
                 
        display_baroverviewchannelservednoshow(series1,series2);
                  },
                  'error':function(data) 
                  {
                        
                       alert(_msgproblemconnect);                   
                  },
                                      
                 });          
        return false;      
    }
      
/////////////////// End   Overview Chart
//
//
//
////////// Begin  Branch Charts
  $('#headertab_b_1').click( function() {
    _currenttab="tab_b_1";
     getbranchcharts(_chartstart,_chartend,currentoffice); 
});
$('#headertab_b_2').click( function() {
    _currenttab="tab_b_2";
     getbranchcharts(_chartstart,_chartend,currentoffice); 
});
$('#headertab_b_3').click( function() {
    _currenttab="tab_b_3";
     getbranchcharts(_chartstart,_chartend,currentoffice); 
});
$('#headertab_b_4').click( function() {
    _currenttab="tab_b_4";
     getbranchcharts(_chartstart,_chartend,currentoffice); 
});
$('#headertab_b_5').click( function() {
    _currenttab="tab_b_5";
     getbranchcharts(_chartstart,_chartend,currentoffice); 
});
$('#headertab_b_6').click( function() {
    _currenttab="tab_b_6";
     getbranchcharts(_chartstart,_chartend,currentoffice); 
});

 getbranchcharts= function(p1,p2,myseloffice){  
   if(typeof _currenttab === 'undefined' )
   {
     _currenttab="tab_b_1"; 
   }
     switch(_currenttab) {
    case "tab_b_1":
    donutpercentnbreticketsbyservice(p1,p2,myseloffice);
     break
   case "tab_b_2":
    baraverageminutesbyservice(p1,p2,myseloffice);
     break
   case "tab_b_3":
    barnbreticketsbyservice(p1,p2,myseloffice);
     break
     case "tab_b_4":
    nbreticketsaverageminutes(p1,p2,myseloffice);
     break
     case "tab_b_5":
    nbreticketsaverageminutesforeachuser(p1,p2,myseloffice);
     break
     case "tab_b_6":
    nbreticketsaverageminutesperformanceby30min(p1,p2,myseloffice);
     break
    default:
    donutpercentnbreticketsbyservice(p1,p2,myseloffice); 
     break
}
          
    return false;
  } 
  
   display_donutpercentnbreticketsbyservice = function(mydata){ 
                 
              $('#tab_b_1_chart1').highcharts({
        chart: {
           plotBackgroundColor: null,
            plotBorderWidth: 1,//null,
            plotShadow: false
        },
        title: {
            text: _titlechart1,
            useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
        },
        tooltip: {
            pointFormat: '{point.label}: <b>{point.percentage:.2f}%</b>',
            useHTML : (_ISRTL == 1) ? true : false
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.label}</b>: {point.percentage:.2f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    },
                    useHTML : (_ISRTL == 1) ? true : false
                }
            }
        },
        
        
        credits: {
            enabled: false
        },
         series: [
         {
         type: 'pie',
           name: '',
            
            data:mydata,
            
         }
        
        ]
    });
    return false;				
    }
  donutpercentnbreticketsbyservice= function(p1,p2,officeid){           
             jQuery.ajax({
                 'dataType':'json',
                 'type':'get',
                'url':baseUrl+'/dashboard/stats/percentticketsbysvc/date1/'+p1+'/date2/'+p2+'/officeId/'+officeid,
                 'cache':false,
                 'beforeSend':function( request )
                     {
                      $('#tab_b_1_chart1').html(_msgbusy);
                     },
                 'success':function(data)
                  {
                    var myserie=$.parseJSON(data);
                    
                   
                    if(myserie[0].y==0)
                    {
                        $('#tab_b_1_chart1').html(_msgnodata);
                    }
                     else{
                     display_donutpercentnbreticketsbyservice(myserie);
                      }   
                  },
                  'error':function(data) 
                  {
                        
                       alert(_msgproblemconnect);                   
                  },
                                      
                 });
            
            
        return false;    
    }
    
  display_baraverageminutesbyservice = function(myseries1,myseries2){ 
                 
              $('#tab_b_2_chart1').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: _titlechart2,
            useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
        },
        subtitle: {
            text: ''
        },
       
        xAxis: {
          categories: [],
          opposite : (_ISRTL == 1) ? true : false,
          labels: {
            useHTML: (_ISRTL == 1) ? Highcharts.hasBidibug : false,
            x : (_ISRTL == 1) ? 100 : -5
          },
            //title: {
//                text: null
//            }

        },
        yAxis: {
            min: 0,
            title: {
                text: _labeltime+' '+_Label_graph_minute_2,
                align: 'high',
                useHTML : (_ISRTL) ? Highcharts.hasBidibug : false
            },
            labels: {
                overflow: 'justify'
            },
            reversed : (_ISRTL == 1) ? true : false
            
        },
        tooltip: {
           // valueSuffix: ' min'
           formatter: function () {
                return '<b>' + this.point.name + '</b><br/>' +
                    this.series.name + ': ' + this.y + _Label_graph_min_2+'<br/>' +
                    _Label_graph_total_2+': ' + this.point.stackTotal+_Label_graph_min_2;
            },

            useHTML : (_ISRTL == 1) ? true : false
        },
      
         plotOptions: {
            
            series: {
                stacking: 'normal'
            },
            bar: {
                 dataLabels: {
                    enabled: true,
                    formatter: function () {
                        return '' + this.point.text+_Label_graph_min_2
                    },
                    useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
                }
            }
            
            
        },
        legend: {
            layout: 'vertical',
            align: (_ISRTL == 1) ? 'left' : 'right',
            verticalAlign: 'top',
            x: (_ISRTL == 1) ? 20 : -20,
            y: 25,
            floating: false,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true,
            useHTML : (_ISRTL == 1) ? true : false
        },
      
        credits: {
            enabled: false
        },
         series: [
         
        
         {
             name: _labelservice,
             color:'rgb(144, 237, 125)',
            data: myseries2,
        }
        ,{
             name: _labelwaiting,
            color:' rgb(124, 181, 236)',
            data:myseries1,
        }
        
        ]
    });
    return false;
}
             
  baraverageminutesbyservice= function(p1,p2,officeid){ 
            
             jQuery.ajax({
                 'dataType':'json',
                 'type':'get',
                 'url':baseUrl+'/dashboard/stats/Averagetimebysvc/date1/'+p1+'/date2/'+p2+'/officeId/'+officeid,
                 'cache':false,
                 'beforeSend':function( request )
                     {
                      $('#tab_b_2_chart1').html(_msgbusy);
                     },
                 'success':function(data)
                  {
                   
                       var series1=$.parseJSON(data.series1);
                      var series2 =$.parseJSON(data.series2);
                 
                  if(series1[0].y==0)
                    {
                        $('#tab_b_2_chart1').html(_msgnodata);
                    }
                    else{
                    display_baraverageminutesbyservice(series1,series2);
                  }
        
                  },
                  'error':function(data) 
                  {
                        
                       alert(_msgproblemconnect);                   
                  },
                                      
                 });           
        return false;     
    }
    
    display_barnbreticketsbyservice = function(mydata){ 
                 
              $('#tab_b_3_chart1').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: _titlechart3,
            useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug :false
        },
        subtitle: {
            text: ''
        },
       
        xAxis: {
          categories: [],
            title: {
             text: _Label_graph_services_2,
             useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
          },
          opposite : (_ISRTL == 1) ? true : false,
          labels: {
            useHTML: (_ISRTL == 1) ? Highcharts.hasBidibug : false,
            x : (_ISRTL == 1) ? 100 : -5
          },
        },
        yAxis: {
            min: 0,
            title: {
                text: _Label_graph_tickets_2,
                align: 'high',
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            },
            labels: {
                overflow: 'justify'
            },
            reversed : (_ISRTL == 1) ? true : false
        },
        tooltip: {
            valueSuffix: ' ',
            useHTML : (_ISRTL == 1) ? true : false
        },
        plotOptions: {
            bar: {
                 dataLabels: {
                    enabled: true,
                    formatter: function () {
                        return '' + this.point.text
                    },
                    useHTML : (_ISRTL == 1) ? true : false
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: (_ISRTL == 1) ? 'left' : 'right',
            verticalAlign: 'top',
            x: (_ISRTL == 1) ? 20 : -20,
            y: 2,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true,
            useHTML : (_ISRTL == 1) ? true : false
        },
        
        credits: {
            enabled: false
        },
         series: [
         {
             name: _Label_graph_tickets_2,
            
            data:mydata,
        }
        
        
        
        
        ]
    });
    return false;
				
    }
               
   barnbreticketsbyservice= function(p1,p2,officeid){ 
            
             jQuery.ajax({
                 'dataType':'json',
                 'type':'get',
                  'url':baseUrl+'/dashboard/stats/displaysvcbytickets/date1/'+p1+'/date2/'+p2+'/officeId/'+officeid,
                 'cache':false,
                 'beforeSend':function( request )
                     {
                      $('#tab_b_3_chart1').html(_msgbusy);
                       $('#summary_tab_b_3_chart1').html("");
                     },
                 'success':function(data)
                  {
                    
                     var myserie=$.parseJSON(data.datas);
                   $('#summary_tab_b_3_chart1').html(data.summary);
                  if(myserie[0].y==0)
                    {
                        $('#tab_b_3_chart1').html(_msgnodata);
                    }
                    else{
                    display_barnbreticketsbyservice(myserie);
                  }
        
                  },
                  'error':function(data) 
                  {
                        
                       alert(_msgproblemconnect);                   
                  },
                                      
                 });          
        return false;  
    } 
    
              
  display_ticketsaverageminutesbyoffice = function(myseries1,myseries2,myseries3){ 
                 
                 $('#tab_b_4_chart1').highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: _titlechart4, //'Performance par service'
            useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
        },
        subtitle: {
            text: ''
        },
        xAxis: [{
            categories: [],
            reversed : (_ISRTL == 1) ? true : false,

        }],
        yAxis: [
        // Primary yAxis
        { 
            gridLineWidth: 0,
            title: {
                text: _labelnbretickets,//Nbre de tickets
                style: {
                    color: Highcharts.getOptions().colors[0]
                },
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            },
            labels: {
                format: '{value} ',
                style: {
                    color: Highcharts.getOptions().colors[0]
                },
                useHTML : (_ISRTL == 1) ? true : false
            },

            //reversed : (_ISRTL == 1) ? true : false

        },
         
        // Secondary yAxis
        { 
            labels: {
                format: '{value}'+_Label_graph_min_2,
                style: {
                    color: Highcharts.getOptions().colors[1]
                },
                x: (_ISRTL == 1) ? 30 : -5,
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            },
            title: {
                text: _labelaveragewaitingtime ,//'Temps moyen en service'
                style: {
                    color: Highcharts.getOptions().colors[1]
                },
                x: (_ISRTL == 1) ? -50 : -10,
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            },
            opposite: true,


        }, 
        // Tertiary yAxis
        { 
            gridLineWidth: 0,
            labels: {
                format: '{value}'+_Label_graph_min_2,
                style: {
                    color: Highcharts.getOptions().colors[2]
                },
                x: (_ISRTL == 1) ? -10 : -5,
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            },
            title: {
                text: _labelaverageservicetime ,//'Temps moyen en service'
                style: {
                    color: Highcharts.getOptions().colors[2]
                },
                x: (_ISRTL == 1) ? -50 : -10,
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            },
            opposite: true
        },
        /*{
            reversed : (_ISRTL == 1) ? true : false
        }*/
            
        ],
        tooltip: {
            shared: true,
            useHTML: (_ISRTL == 1) ? true : false
        },
        credits: {
            enabled: false
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: (_ISRTL == 1) ? 20 : -20,
            y: 2,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true,
            useHTML : (_ISRTL == 1) ? true : false
        },

       /*legend: {
            layout: 'vertical',
            align: 'left',
            x: (_ISRTL == 1) ? -30 : 30,
            verticalAlign: 'top',
            y: 2,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF',
            useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
        },*/
         series: [
         {
             name:  _labelnbretickets,//Nbre de tickets
           
            type: 'column',
            yAxis: 0,
            data:myseries1,
        },
        {
             name: _labelaveragewaitingtime ,//'Temps moyen en attente'
           
            type: 'spline',
            yAxis: 1,
            data:myseries2,
            marker: {
                enabled: false
            },
            dashStyle: 'shortdot',
            tooltip: {
                valueSuffix:_Label_graph_min_2,
                useHTML: (_ISRTL == 1) ? Highcharts.hasBidibug : false
            }
        },
        {
             name: _labelaverageservicetime ,//'Temps moyen en service'
           
             type: 'spline',
            yAxis: 2,
            data:myseries3,
            marker: {
                enabled: false
            },
             tooltip: {
                valueSuffix:_Label_graph_min_2,
                useHTML: (_ISRTL == 1) ? Highcharts.hasBidibug : false
            }
        }
        
        
        ]
    });
                 
                 
   
    return false;
				
			}
               
 nbreticketsaverageminutes= function(p1,p2,officeid){ 
            
             jQuery.ajax({
                 'dataType':'json',
                 'type':'get',
                 'url':baseUrl+'/dashboard/stats/Nbreticketsaverageminutes/date1/'+p1+'/date2/'+p2+'/officeId/'+officeid,
                 'cache':false,
                 'beforeSend':function( request )
                     {
                      $('#tab_b_4_chart1').html(_msgbusy);
                     },
                 'success':function(data)
                  {
                    
                       var series1=$.parseJSON(data.series1);
                      var series2 =$.parseJSON(data.series2);
                         var series3 =$.parseJSON(data.series3);
                         
                          if(series1[0].y==0)
                  {
                      $('#tab_b_4_chart1').html(_msgnodata);
                  }
                  
                  else{
                    display_ticketsaverageminutesbyoffice(series1,series2,series3);
                  }
                  
        
                  },
                  'error':function(data) 
                  {
                        
                       alert(_msgproblemconnect);                      
                  },
                                      
                 });          
        return false;  
    }
    display_nbreticketsaverageminutesforeachuser = function(myseries1,myseries2,myseries3){ 
                 
                 $('#tab_b_5_chart1').highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: _titlechart5, //'Performance par service'
            useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
        },
        subtitle: {
            text: ''
        },
        xAxis: [{
            categories: [],
            reversed : (_ISRTL == 1) ? true : false,
            useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
        }],
        yAxis: [
        // Primary yAxis
        { 
            gridLineWidth: 0,
            title: {
                text: _labelnbretickets,//Nbre de tickets
                style: {
                    color: Highcharts.getOptions().colors[0]
                },
                useHTML : (_ISRTL == 1 ) ? Highcharts.hasBidibug : false
            },
            labels: {
                format: '{value} ',
                style: {
                    color: Highcharts.getOptions().colors[0]
                },
            }

        },
         
        // Secondary yAxis
        { 
            labels: {
                format: '{value}'+_Label_graph_min_2,
                style: {
                    color: Highcharts.getOptions().colors[1]
                },
                x : (_ISRTL == 1) ? 30 : -5,
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            },
            title: {
                text: _labelaveragewaitingtime ,//'Temps moyen en service'
                style: {
                    color: Highcharts.getOptions().colors[1]
                },
                x : (_ISRTL == 1) ? -50 : 5,
                useHTML : (_ISRTL == 1 ) ? Highcharts.hasBidibug : false
            },
            opposite: true

        }, 
        // Tertiary yAxis
        { 
            gridLineWidth: 0,
            
            labels: {
                format: '{value}'+_Label_graph_min_2,
                style: {
                    color: Highcharts.getOptions().colors[2]
                },
                x: (_ISRTL==1) ? -10 : -5,
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            },
            title: {
                text: _labelaverageservicetime ,//'Temps moyen en service'
                style: {
                    color: Highcharts.getOptions().colors[2]
                },
                x: (_ISRTL == 1) ? -50 : 5,
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            },
            opposite: true
        }],
        tooltip: {
            shared: true,
            useHTML: (_ISRTL == 1) ? true : false
        },
        credits: {
            enabled: false
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: (_ISRTL == 1) ? 10 : -20,
            y: 2,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true,
            useHTML : (_ISRTL == 1) ? true : false
        },

       /* legend: {
            layout: 'vertical',
            align: 'left',
            x: 30,
            verticalAlign: 'top',
            y: 2,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },*/
         series: [
         {
             name:  _labelnbretickets,//Nbre de tickets
           
            type: 'column',
            yAxis: 0,
            data:myseries1,
        },
        {
             name: _labelaveragewaitingtime ,//'Temps moyen en attente'
           
            type: 'spline',
            yAxis: 1,
            data:myseries2,
            marker: {
                enabled: false
            },
            dashStyle: 'shortdot',
            tooltip: {
                valueSuffix: _Label_graph_min_2,
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            }
        },
        {
             name: _labelaverageservicetime ,//'Temps moyen en service'
           
             type: 'spline',
            yAxis: 2,
            data:myseries3,
            marker: {
                enabled: false
            },
             tooltip: {
                valueSuffix: _Label_graph_min_2,
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            }
        }
        
        
        ]
    });
    return false;				
    }
  
   nbreticketsaverageminutesforeachuser= function(p1,p2,officeid){ 
            
             jQuery.ajax({
                 'dataType':'json',
                 'type':'get',
                 'url':baseUrl+'/dashboard/stats/Performanceofficebyuser/date1/'+p1+'/date2/'+p2+'/officeId/'+officeid,
                 'cache':false,
                 'beforeSend':function( request )
                     {
                      $('#tab_b_5_chart1').html(_msgbusy);
                      $('#summary_tab_b_5_chart1').html("");
                     },
                 'success':function(data)
                  {
                    
                       var series1=$.parseJSON(data.series1);
                      var series2 =$.parseJSON(data.series2);
                         var series3 =$.parseJSON(data.series3);
                         $('#summary_tab_b_5_chart1').html(data.summary);
                         
                          if(series1[0].y==0)
                  {
                      $('#tab_b_5_chart1').html(_msgnodata);
                  }
                  
                  else{
                    display_nbreticketsaverageminutesforeachuser(series1,series2,series3);
                  }
                  
        
                  },
                  'error':function(data) 
                  {
                        
                       alert(_msgproblemconnect);                      
                  },
                                      
                 });          
        return false;   
    }
    
  display_ticketsaverageminutesbyofficeperformanceby30min = function(myseries1,myseries2,myseries3){ 
                 
                 $('#tab_b_6_chart1').highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text:_titlechart6,// 'Performance par 30min'
            useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
        },
        subtitle: {
            text: ''
        },
        xAxis: [{
            categories: [],
            reversed : (_ISRTL == 1) ? true : false
        }],
        yAxis: [
        // Primary yAxis
        { 
            gridLineWidth: 0,
            title: {
                text: _labelnbretickets,//Nbre de tickets
                style: {
                    color: Highcharts.getOptions().colors[0]
                },
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            },
            labels: {
                format: '{value} ',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            }

        },
         
        // Secondary yAxis
        { 
            labels: {
                format: '{value}'+_Label_graph_min_2,
                style: {
                    color: Highcharts.getOptions().colors[1]
                },
                x: (_ISRTL == 1) ? 30 : -5,
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            },
            title: {
                text:_labelaveragewaitingtime ,//'Temps moyen en attente'
                style: {
                    color: Highcharts.getOptions().colors[1]
                },
                x : (_ISRTL ==1 ) ? -50 : -10,
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            },
            opposite: true

        }, 
        // Tertiary yAxis
        { 
            gridLineWidth: 0,
            
            labels: {
                format: '{value}'+_Label_graph_min_2,
                style: {
                    color: Highcharts.getOptions().colors[2]
                },
                x : (_ISRTL == 1) ? -10 : -5,
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            },
            title: {
                text:_labelaverageservicetime ,//'Temps moyen en service'
                style: {
                    color: Highcharts.getOptions().colors[2]
                },
                x : (_ISRTL == 1) ? -50 : -10,
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            },
            opposite: true
        }],
        tooltip: {
            shared: true,
            useHTML : (_ISRTL == 1) ? true : false
        },
        credits: {
            enabled: false
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: (_ISRTL == 1) ? 10 : -20,
            y: 2,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true,
            useHTML : (_ISRTL == 1) ? true : false
        },

        /*legend: {
            layout: 'vertical',
            align: 'left',
            x: 30,
            verticalAlign: 'top',
            y: 2,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },*/
         series: [
         {
             name: _labelnbretickets,//Nbre de tickets
           
            type: 'column',
            yAxis: 0,
            data:myseries1,
        },
        {
             name: _labelaveragewaitingtime ,//'Temps moyen en attente'
           
            type: 'spline',
            yAxis: 1,
            data:myseries2,
            marker: {
                enabled: false
            },
            dashStyle: 'shortdot',
            tooltip: {
                valueSuffix: _Label_graph_min_2,
                useHTML : (_ISRTL == 1 ) ? Highcharts.hasBidibug : false
            }
        },
        {
             name: _labelaverageservicetime ,//'Temps moyen en service'
           
             type: 'spline',
            yAxis: 2,
            data:myseries3,
            marker: {
                enabled: false
            },
             tooltip: {
                valueSuffix: _Label_graph_min_2,
                useHTML : (_ISRTL == 1 ) ? Highcharts.hasBidibug : false
            }
        }
        
        
        ]
    });
                 
                 
   
    return false;
				
			}
    nbreticketsaverageminutesperformanceby30min= function(p1,p2,officeid){ 
            
             jQuery.ajax({
                 'dataType':'json',
                 'type':'get',
                 'url':baseUrl+'/dashboard/stats/rushhourBy30min/date1/'+p1+'/date2/'+p2+'/officeId/'+officeid,
                 'cache':false,
                 'beforeSend':function( request )
                     {
                      $('#tab_b_6_chart1').html(_msgbusy);
                     },
                 'success':function(data)
                  {
                    
                       var series1=$.parseJSON(data.series1);
                      var series2 =$.parseJSON(data.series2);
                         var series3 =$.parseJSON(data.series3);
                         if(series1[0].y==0)
                  {
                      $('#tab_b_6_chart1').html(_msgnodata);
                  }
                  else{
                    display_ticketsaverageminutesbyofficeperformanceby30min(series1,series2,series3);
                  }
                  
        
                  },
                  'error':function(data) 
                  {
                        
                       alert(_msgproblemconnect);                       
                  },
                                      
                 });      
        return false;    
    }
///////////End Branch Charts
           
 /////////////////// Begin  User Charts

$('#headertab_u_1').click( function() {
    _currenttab="tab_u_1";
     getusercharts(_chartstart,_chartend,_user); 
});
$('#headertab_u_2').click( function() {
    _currenttab="tab_u_2";
     getusercharts(_chartstart,_chartend,_user); 
});
$('#headertab_u_3').click( function() {
    _currenttab="tab_u_3";
     getusercharts(_chartstart,_chartend,_user); 
});
$('#headertab_u_4').click( function() {
    _currenttab="tab_u_4";
     getusercharts(_chartstart,_chartend,_user); 
});

 getusercharts= function(p1,p2,userid){  
   if(typeof _currenttab === 'undefined' )
   {
     _currenttab="tab_u_1"; 
   }
     switch(_currenttab) {
    case "tab_u_1":
    stackbaraverageminutesforuser(p1,p2,userid);
     break
   case "tab_u_2":
   barnbreticketsbyserviceforuser(p1,p2,userid);
     break
   case "tab_u_3":
   nbreticketsaverageminutesforuser(p1,p2,userid);
     break
   case "tab_u_4":
  nbreticketsaverageminutesforuserperformanceby30min(p1,p2,userid);
     break
    default:
     stackbaraverageminutesforuser(p1,p2,userid); 
     break
}
          
    return false;
  }


      display_stackbaraverageminutesforuser = function(myseries1,myseries2){
                 
              $('#tab_u_1').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: _titlechart1,
            useHTML: (_ISRTL == 1) ? Highcharts.hasBidibug : false
        },
        subtitle: {
            text: ''
        },
       
        xAxis: {
          categories: [],
          opposite : (_ISRTL == 1) ? true : false,
          labels: {
            useHTML: (_ISRTL == 1) ? Highcharts.hasBidibug : false,
            x : (_ISRTL == 1) ? 100 : -5
          },
            //title: {
//                text: null
//            }
        },
        yAxis: {
            min: 0,
            title: {
                text:  _labeltime+' '+_Label_graph_minute_3,
                align: 'high',
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            },
            labels: {
                overflow: 'justify'
            },
            reversed : (_ISRTL == 1) ? true : false
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.point.name + '</b><br/>' +
                    this.series.name + ': ' + this.y + _Label_graph_min_3+'<br/>' +
                    _Label_graph_total_3+': ' + this.point.stackTotal+_Label_graph_min_3;
            },
            useHTML : (_ISRTL == 1) ? true : false
        },
        plotOptions: {
            
            series: {
                stacking: 'normal'
            },
            bar: {
                 dataLabels: {
                    enabled: true,
                    formatter: function () {
                        return '' + this.point.text+_Label_graph_min_3
                    },
                    useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
                }
            }
            
            
        },
        legend: {
            layout: 'vertical',
            align: (_ISRTL == 1) ? 'left' : 'right',
            verticalAlign: 'top',
            reversed: true,
            x: (_ISRTL == 1) ? 40 : -40,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true,
            useHTML : (_ISRTL == 1) ? true : false
        },
        credits: {
            enabled: false
        },
         series: [
         
        
         {
             name: _labelservice,
             color:'rgb(144, 237, 125)',
            data: myseries2,
        }
        ,{
             name: _labelwaiting,
            color:' rgb(124, 181, 236)',
            data:myseries1,
        }
        
        ]
    });
    return false;
				
			}
               
         stackbaraverageminutesforuser= function(p1,p2,userid){ 
            
             jQuery.ajax({
                 'dataType':'json',
                 'type':'get',
                 'url':baseUrl+'/dashboard/stats/Averagetimeforuser/date1/'+p1+'/date2/'+p2+'/userid/'+userid,
                 'cache':false,
                 'beforeSend':function( request )
                     {
                        $('#tab_u_1').html(_msgbusy);
                     },
                 'success':function(data)
                  {
                    
                       var series1=$.parseJSON(data.series1);
                      var series2 =$.parseJSON(data.series2);
                 
                  if(series1[0].y==0)
                    {
                        $('#tab_u_1').html(_msgnodata);
                    }
                    else{
                    display_stackbaraverageminutesforuser(series1,series2);
                  }
        
                  },
                  'error':function(data) 
                  {
                        
                       alert(_msgproblemconnect);                   
                  },
                                      
                 });
            
            
        return false;
            
              
       
    }
     
              display_barnbreticketsbyserviceforuser = function(mydata){ 
                 
              $('#tab_u_2').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: _titlechart2
        },
        subtitle: {
            text: ''
        },
       
        xAxis: {
          categories: [],
            title: {
             text: _Label_graph_services_3,
             x : (_ISRTL == 1) ? -50 : -10, 
             useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
          },
          labels: {
            useHTML: (_ISRTL == 1) ? Highcharts.hasBidibug : false,
            x : (_ISRTL == 1) ? 100 : -5
          },
          opposite : (_ISRTL ==1) ? true : false
        },
        yAxis: {
            min: 0,
            title: {
                text: _Label_graph_tickets_3,
                align: 'high',
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            },
            labels: {
                overflow: 'justify'
            },
            reversed : (_ISRTL == 1) ? true : false
        },
        tooltip: {
            valueSuffix: ' ',
            useHTML : (_ISRTL == 1 )? true : false
        },
        plotOptions: {
            bar: {
                 dataLabels: {
                    enabled: true,
                    formatter: function () {
                        return '' + this.point.text
                    },
                    useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: (_ISRTL == 1) ? 'left' : 'right',
            verticalAlign: 'top',
            reversed: true,
            x: (_ISRTL == 1) ? 40 : -40,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true,
            useHTML : (_ISRTL == 1) ? true : false
        },
        credits: {
            enabled: false
        },
         series: [
         {
             name:_Label_graph_tickets_3,
             useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false,
            
            data:mydata,
        }
        
        
        
        
        ]
    });
    return false;
				
			}
               
         barnbreticketsbyserviceforuser= function(p1,p2,userid){ 
            
             jQuery.ajax({
                 'dataType':'json',
                 'type':'get',
                 'url':baseUrl+'/dashboard/stats/currentusernbreticketsbysvc/date1/'+p1+'/date2/'+p2+'/userid/'+userid,
                 'cache':false,
                 'beforeSend':function( request )
                     {
                       $('#tab_u_2').html(_msgbusy);
                        $('#summary_tab_u_2_chart2').html("");
                     },
                 'success':function(data)
                  {
                    
                    
                     
                      var myserie=$.parseJSON(data.datas);
                   $('#summary_tab_u_2_chart2').html(data.summary);
                   
                  if(myserie[0].y==0)
                    {
                        $('#tab_u_2').html(_msgnodata);
                    }
                    else{
                    display_barnbreticketsbyserviceforuser(myserie);
                  }
        
                  },
                  'error':function(data) 
                  {
                        
                       alert(_msgproblemconnect);                   
                  },
                                      
                 });
            
            
        return false;
            
              
       
    }
            
            
            display_ticketsaverageminutesbyofficeforuser = function(myseries1,myseries2,myseries3){ 
                 
                 $('#tab_u_3').highcharts({
         chart: {
            zoomType: 'xy'
        },
        title: {
            text: _titlechart3,
            useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
        },
        subtitle: {
            text: ''
        },
        xAxis: [{
            categories: [],
            reversed : (_ISRTL == 1) ? true : false
        }],
        yAxis: [
        // Primary yAxis
        { 
            gridLineWidth: 0,
            title: {
                text:_labelnbretickets,// 'Nbre de tickets',
                style: {
                    color: Highcharts.getOptions().colors[0]
                },
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            },
            labels: {
                format: '{value} ',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            }

        },
         
        // Secondary yAxis
        { 
            labels: {
                format:'{value}'+_Label_graph_min_3,
                style: {
                    color: Highcharts.getOptions().colors[1]
                },
                x : (_ISRTL == 1) ? 30 : -5,
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            },
            title: {
                text:_labelaveragewaitingtime,// 'Temps moyen en attente',
                style: {
                    color: Highcharts.getOptions().colors[1]
                },
                x : (_ISRTL == 1) ? -50 : 5,
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            },
            opposite: true

        }, 
        // Tertiary yAxis
        { 
            gridLineWidth: 0,
            
            labels: {
                format: '{value}'+_Label_graph_min_3,
                style: {
                    color: Highcharts.getOptions().colors[2]
                },
                x: (_ISRTL == 1) ? -10 : -5,
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            },
            title: {
                text:_labelaverageservicetime,// 'Temps moyen en service',
                style: {
                    color: Highcharts.getOptions().colors[2]
                },
                x: (_ISRTL == 1) ? -50 : 5,
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            },
            opposite: true
        }],
        tooltip: {
            shared: true,
            useHTML : (_ISRTL == 1) ? true : false
        },
        credits: {
            enabled: false
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: (_ISRTL == 1) ? 40 : 30,
            y: 2,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true,
            useHTML : (_ISRTL == 1) ? true : false
        },
       /* legend: {
            layout: 'vertical',
            align: 'left',
            x: 30,
            verticalAlign: 'top',
            y: 2,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },*/
         series: [
         {
             name:_labelnbretickets,// 'Nbre de tickets',
           
            type: 'column',
            yAxis: 0,
            data:myseries1,
        },
        {
             name:_labelaveragewaitingtime,// 'Temps moyen en attente',
           
            type: 'spline',
            yAxis: 1,
            data:myseries2,
            marker: {
                enabled: false
            },
            dashStyle: 'shortdot',
            tooltip: {
                valueSuffix:_Label_graph_min_3, 
                useHTML: (_ISRTL == 1) ? Highcharts.hasBidibug : false
            }
        },
        {
             name: _labelaverageservicetime,//'Temps moyen en service',
           
             type: 'spline',
            yAxis: 2,
            data:myseries3,
            marker: {
                enabled: false
            },
             tooltip: {
                valueSuffix:_Label_graph_min_3, 
                useHTML: (_ISRTL == 1) ? Highcharts.hasBidibug : false
            }
        }
        
        
        ]
    });
                 
                 
   
    return false;
				
			}
               
         nbreticketsaverageminutesforuser = function(p1,p2,userid){ 
            
             jQuery.ajax({
                 'dataType':'json',
                 'type':'get',
                 'url':baseUrl+'/dashboard/stats/Nbreticketsaverageminutesforuser/date1/'+p1+'/date2/'+p2+'/userid/'+userid,
                 'cache':false,
                 'beforeSend':function( request )
                     {
                       $('#tab_u_3').html(_msgbusy);
                     },
                 'success':function(data)
                  {
                    
                       var series1=$.parseJSON(data.series1);
                      var series2 =$.parseJSON(data.series2);
                         var series3 =$.parseJSON(data.series3);
                  if(series1[0].y==0 )
                  {
                      $('#tab_u_3').html(_msgnodata);
                  }
                  else{
                    display_ticketsaverageminutesbyofficeforuser(series1,series2,series3);
                  }
                  
                 
        
                  },
                  'error':function(data) 
                  {
                        
                      alert(_msgproblemconnect);                 
                  },
                                      
                 });
            
            
        return false;
            
              
       
    }
    
      
     
    display_ticketsaverageminutesbyofficeforuserperformanceby30min = function(myseries1,myseries2,myseries3){ 

                 $('#tab_u_4').highcharts({
         chart: {
            zoomType: 'xy'
        },
        title: {
            text: _titlechart4,
            useHTML : (_ISRTL == 1 ) ? Highcharts.hasBidibug : false
        },
        subtitle: {
            text: ''
        },
        xAxis: [{
            categories: [],
            reversed : (_ISRTL == 1 )? true : false
        }],
        yAxis: [
        // Primary yAxis
        { 
            gridLineWidth: 0,
            title: {
                text:_labelnbretickets,// 'Nbre de tickets',
                style: {
                    color: Highcharts.getOptions().colors[0]
                },
                useHTML : (_ISRTL == 1 ) ? Highcharts.hasBidibug : false
            },
            labels: {
                format: '{value} ',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            }

        },
         
        // Secondary yAxis
        { 
            labels: {
                format: '{value}'+_Label_graph_min_3,
                style: {
                    color: Highcharts.getOptions().colors[1]
                },
                x : (_ISRTL == 1) ? 30 : -5,
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            },
            title: {
                text: _labelaveragewaitingtime,//'Temps moyen en attente',
                style: {
                    color: Highcharts.getOptions().colors[1]
                },
                x : (_ISRTL == 1) ? -50 : 5,
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            },
            opposite: true

        }, 
        // Tertiary yAxis
        { 
            gridLineWidth: 0,
            
            labels: {
                format: '{value}'+_Label_graph_min_3,
                style: {
                    color: Highcharts.getOptions().colors[2]
                },
                x: (_ISRTL == 1) ? -10 : -5,
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            },
            title: {
                text:_labelaverageservicetime,// 'Temps moyen en service',
                style: {
                    color: Highcharts.getOptions().colors[2]
                },
                x : (_ISRTL == 1) ? -50 : 5,
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            },
            opposite: true
        }],
        tooltip: {
            shared: true,
            useHTML : (_ISRTL == 1) ? true : false
        },
        credits: {
            enabled: false
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: (_ISRTL == 1) ? 40 : 30,
            y: 2,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true,
            useHTML : (_ISRTL == 1) ? true : false
        },
        /*legend: {
            layout: 'vertical',
            align: 'left',
            x: 30,
            verticalAlign: 'top',
            y: 2,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },*/
         series: [
         {
             name:_labelnbretickets,// 'Nbre de tickets',
           
            type: 'column',
            yAxis: 0,
            data:myseries1,
        },
        {
             name:_labelaveragewaitingtime,// 'Temps moyen en attente',
           
            type: 'spline',
            yAxis: 1,
            data:myseries2,
            marker: {
                enabled: false
            },
            dashStyle: 'shortdot',
            tooltip: {
                valueSuffix:_Label_graph_min_3, 
                useHTML: (_ISRTL == 1) ? Highcharts.hasBidibug : false
            }
        },
        {
             name:_labelaverageservicetime,// 'Temps moyen en service',
           
             type: 'spline',
            yAxis: 2,
            data:myseries3,
            marker: {
                enabled: false
            },
             tooltip: {
                valueSuffix:_Label_graph_min_3, 
                useHTML: (_ISRTL == 1) ? Highcharts.hasBidibug : false
            }
        }
        
        
        ]
    });
                 
                 
   
    return false;
				
			}
     nbreticketsaverageminutesforuserperformanceby30min = function(p1,p2,userid){ 

             jQuery.ajax({
                 'dataType':'json',
                 'type':'get',
                 'url':baseUrl+'/dashboard/stats/rushhourby30minforuser/date1/'+p1+'/date2/'+p2+'/userid/'+userid,
                 'cache':false,
                 'beforeSend':function( request )
                     {
                       $('#tab_u_4').html(_msgbusy);
                     },
                 'success':function(data)
                  {
                    
                       var series1=$.parseJSON(data.series1);
                      var series2 =$.parseJSON(data.series2);
                         var series3 =$.parseJSON(data.series3);
                  if(series1[0].y==0)
                  {
                      $('#tab_u_4').html(_msgnodata);
                  }
                  else{
                    display_ticketsaverageminutesbyofficeforuserperformanceby30min(series1,series2,series3);
                  }
                  
        
                  },
                  'error':function(data) 
                  {
                        
                      alert(_msgproblemconnect);                    
                  },
                                      
                 });
            
            
        return false;
            
              
       
    }
/////////////////// End  user Charts


      ///////////////////    Survey
  surveyoverview = function(p1,p2){
       overviewsurveydata(p1,p2);        
				return false;
			}    
                        
                        
 display_overviewsurvey = function(mydata){ 
                 
              $('#chart_div').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: '',
            useHTML: (_ISRTL == 1 ) ? Highcharts.hasBidibug : false,
        },
        subtitle: {
            text: ''
        },
       
        xAxis: {
            reversed: (_ISRTL == 1) ? true : false,
          categories: [],
            title: {
             text: _labelratingfrequency,
             useHTML : (_ISRTL == 1) ? true :false
          }
        },
        yAxis: {
            opposite: (_ISRTL == 1) ? true : false,
            min: 0,
            title: {
                text: _label_responses,
                align: 'middle',
                useHTML : (_ISRTL == 1) ? true : false,
                //y: (_ISRTL == 1) ? 150 : 100,
            },
            labels: {
                overflow: 'justify'
            }
        },
       legend: {
            enabled: false
        },
         tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> <br/>',
            useHTML: (_ISRTL == 1) ? true : false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y}'
                }
            }
        },
        credits: {
            enabled: false
        },
         series: [
         {
             name: _label_responses,
             
          //  colorByPoint: true,
            data:mydata,
        }
        
        ]
    });
    return false;
				
			}
                        
                        
 display_overviewspidometer = function(mydata){ 

    $('#spidometer_div').highcharts({

        chart: {
            type: 'gauge',
            plotBackgroundColor: null,
            plotBackgroundImage: null,
            plotBorderWidth: 0,
            plotShadow: false
        },

        title: {
            text: '',
            useHTML: (_ISRTL == 1) ? Highcharts.hasBidibug : false
        },

        pane: {
            startAngle: -150,
            endAngle: 150,
            background: [{
                backgroundColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                    stops: [
                        [0, '#FFF'],
                        [1, '#333']
                    ]
                },
                borderWidth: 0,
                outerRadius: '109%'
            }, {
                backgroundColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                    stops: [
                        [0, '#333'],
                        [1, '#FFF']
                    ]
                },
                borderWidth: 1,
                outerRadius: '107%'
            }, {
                // default background
            }, {
                backgroundColor: '#DDD',
                borderWidth: 0,
                outerRadius: '105%',
                innerRadius: '103%'
            }]
        },

        // the value axis
        yAxis: {
            min: -100,
            max: 100,
            reversed: (_ISRTL == 1) ? true : false,
            minorTickInterval: 'auto',
            minorTickWidth: 1,
            minorTickLength: 10,
            minorTickPosition: 'inside',
            minorTickColor: '#666',

            tickPixelInterval: 30,
            tickWidth: 2,
            tickPosition: 'inside',
            tickLength: 10,
            tickColor: '#666',
            labels: {
                step: 2,
                rotation: 'auto',
                useHTML: (_ISRTL == 1) ? true : false
            },
            title: {
                text: _labelscorenps,
                useHTML : (_ISRTL == 1) ? Highcharts.hasBidibug : false
            },
            plotBands: [
                {
                from:(_ISRTL == 1) ? 0 :-100,
                to: (_ISRTL == 1) ? -100 :0,
                color: '#DF5353' // red
            },
             {
                from: (_ISRTL == 1) ? 50 :0,
                to: (_ISRTL == 1) ? 0 :50,
                color: '#DDDF0D' // yellow
            }, {
                from: (_ISRTL == 1) ? 100 :50,
                to: (_ISRTL == 1) ? 50 :100,
                color: '#55BF3B' // green
            } ]
        },
        plotOptions : {
            series : {
                dataLabels: {
                    useHTML: (_ISRTL == 1) ? true : false
                }
            }
        },
        credits: {
            enabled: false
        },
        tooltip: {
            useHTML: (_ISRTL == 1 )? true : false
        },

        series: [{
            name: _Label_graph_NPS,
            data: [mydata],
            tooltip: {
                valueSuffix: ' ',
                useHTML: (_ISRTL == 1) ? true : false
            }
        }]

    });
    
     return false;
}         
         overviewsurveydata= function(p1,p2){ 
           $('#survey_loading_error').html(_msgbusy); 
           $('#survey_display').addClass("hidden");
           $('#survey_loading_error').removeClass("hidden");
             jQuery.ajax({
                 'dataType':'json',
                 'type':'get',
                 'url':baseUrl+'/dashboard/survey/Overview/date1/'+p1+'/date2/'+p2,
                 'cache':false,
                 'beforeSend':function( request )
                     {
                       
                        
                     },
                 'success':function(data)
                  {
                    
                    
                     
                      var myserie=$.parseJSON(data.scores_responses_series);
                      var mynps_data=$.parseJSON(data.data_nps_array);
                     
                      if(mynps_data.detractors>0 || mynps_data.passives>0 || mynps_data.promoters>0)
                      {
                          $('#survey_display').removeClass("hidden");
                    $('#survey_loading_error').addClass("hidden");
                          display_overviewsurvey(myserie);
                        
                   $('#progress_div').html(mynps_data.html);
                   display_overviewspidometer(mynps_data.percentnps);
                   
                    
                    
                      }
                      else
                      {
                          $('#survey_loading_error').html(_msgnodata); 
                      }
                      
                 
        
                  },
                  'error':function(data) 
                  {
                        
                       alert(_msgproblemconnect);                   
                  },
                                      
                 });
            
            
        return false;
            
              
       
    }
      
      
      officesurveydata= function(p1,p2,mycuroffice){ 
           $('#survey_loading_error').html(_msgbusy); 
           $('#survey_display').addClass("hidden");
           $('#survey_loading_error').removeClass("hidden");
           
             jQuery.ajax({
                 'dataType':'json',
                 'type':'get',
                 'url':baseUrl+'/dashboard/survey/Office/date1/'+p1+'/date2/'+p2+'/office/'+mycuroffice,
                 'cache':false,
                 'beforeSend':function( request )
                     {
                       
                        
                     },
                 'success':function(data)
                  {
                    
                    
                     
                      var myserie=$.parseJSON(data.scores_responses_series);
                      var mynps_data=$.parseJSON(data.data_nps_array);
                     
                      if(mynps_data.detractors>0 || mynps_data.passives>0 || mynps_data.promoters>0)
                      {
                          $('#survey_display').removeClass("hidden");
                    $('#survey_loading_error').addClass("hidden");
                          display_overviewsurvey(myserie);
                        
                   $('#progress_div').html(mynps_data.html);
                   display_overviewspidometer(mynps_data.percentnps);
                   
                    
                    
                      }
                      else
                      {
                          $('#survey_loading_error').html(_msgnodata); 
                      }
                      
                 
        
                  },
                  'error':function(data) 
                  {
                        
                       alert(_msgproblemconnect);                   
                  },
                                      
                 });
            
            
        return false;
            
              
       
    }
    usersurveydata= function(p1,p2,myuser){ 
           $('#survey_loading_error').html(_msgbusy); 
           $('#survey_display').addClass("hidden");
           $('#survey_loading_error').removeClass("hidden");
           
             jQuery.ajax({
                 'dataType':'json',
                 'type':'get',
                 'url':baseUrl+'/dashboard/survey/user_survey/date1/'+p1+'/date2/'+p2+'/user/'+myuser,
                 'cache':false,
                 'beforeSend':function( request )
                     {
                       
                        
                     },
                 'success':function(data)
                  {
                    
                    
                     
                      var myserie=$.parseJSON(data.scores_responses_series);
                      var mynps_data=$.parseJSON(data.data_nps_array);
                     
                      if(mynps_data.detractors>0 || mynps_data.passives>0 || mynps_data.promoters>0)
                      {
                          $('#survey_display').removeClass("hidden");
                    $('#survey_loading_error').addClass("hidden");
                          display_overviewsurvey(myserie);
                        
                   $('#progress_div').html(mynps_data.html);
                   display_overviewspidometer(mynps_data.percentnps);
                   
                    
                    
                      }
                      else
                      {
                          $('#survey_loading_error').html(_msgnodata); 
                      }
                      
                 
        
                  },
                  'error':function(data) 
                  {
                        
                       alert(_msgproblemconnect);                   
                  },
                                      
                 });
            
            
        return false;
            
              
       
    }
      /////////////////// End    survey       
            
            
    return {
        //main function to initiate the module
        init: function () {
          
            
        }
    };

}();


  


      


  });
            
            
            
            
            
            