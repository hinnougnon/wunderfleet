
<?php
echo "<script>
      visitor_step=".(isset(Yii::app()->session['form_step'])?(Yii::app()->session['form_step']):1).";
</script>";
?>


<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> Wunder
    <small>Get your fleet sharing ready</small>
</h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">

        <div class="portlet light bordered" id="form_wizard_1">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-red"></i>
                    <span class="caption-subject font-red bold uppercase"> Sign up -
                                                <span class="step-title"> Step 1 of 4 </span>
                                            </span>
                </div>

            </div>
            <div class="portlet-body form">
                <form class="form-horizontal" action="#" id="submit_form" method="POST">
                    <div class="form-wizard">
                        <div class="form-body">
                            <ul class="nav nav-pills nav-justified steps">
                                <li>
                                    <a href="#tab1" data-toggle="tab" class="step">
                                        <span class="number"> 1 </span>
                                        <span class="desc">
                                                                    <i class="fa fa-check"></i> Personal  </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab2" data-toggle="tab" class="step">
                                        <span class="number"> 2 </span>
                                        <span class="desc">
                                                                    <i class="fa fa-check"></i> Address  </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab3" data-toggle="tab" class="step active">
                                        <span class="number"> 3 </span>
                                        <span class="desc">
                                                                    <i class="fa fa-check"></i> Payment </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab4" data-toggle="tab" class="step">
                                        <span class="number"> 4 </span>
                                        <span class="desc">
                                                                    <i class="fa fa-check"></i> Confirmation </span>
                                    </a>
                                </li>
                            </ul>
                            <div id="bar" class="progress progress-striped" role="progressbar">
                                <div class="progress-bar progress-bar-success"> </div>
                            </div>
                            <div class="tab-content">
                                <div class="alert alert-danger display-none">
                                    <button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. </div>
                                <div class="alert alert-success display-none">
                                    <button class="close" data-dismiss="alert"></button> Your form validation is successful! </div>
                                <div class="tab-pane active" id="tab1">
                                    <h3 class="block">Provide your personal information</h3>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Firstname
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control"   name="firstname" id="firstname" value="<?php echo isset( Yii::app()->session['firstname'])?Yii::app()->session['firstname']:'' ?>"/>
                                            <span class="help-block"> Provide your firstname. </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Lastname
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="lastname" id="lastname" value="<?php echo isset(Yii::app()->session['lastname'])?Yii::app()->session['lastname']:'' ?>"/>
                                            <span class="help-block"> Provide your lastname. </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Phone Number
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="telephone" id="telephone"  value="<?php echo isset(Yii::app()->session['telephone'])?Yii::app()->session['telephone']:'' ?>"/>
                                            <span class="help-block"> Provide your phone number. </span>
                                        </div>
                                    </div>
                                    <!--<div class="form-group">
                                        <label class="control-label col-md-3">Confirm Password
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="password" class="form-control" name="rpassword" />
                                            <span class="help-block"> Confirm your password </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Email
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="email" />
                                            <span class="help-block"> Provide your email address </span>
                                        </div>
                                    </div>-->
                                </div>
                                <div class="tab-pane" id="tab2">
                                    <h3 class="block">Provide your address details</h3>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Address
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="address" value="<?php echo isset(Yii::app()->session['address'])?Yii::app()->session['address']:'' ?>"/>
                                            <span class="help-block"> Provide your street address </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">House
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="house" value="<?php echo isset(Yii::app()->session['house'])?Yii::app()->session['house']:'' ?>"/>
                                            <span class="help-block"> Provide your house number </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Zip
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="zip" value="<?php echo isset(Yii::app()->session['zip'])?Yii::app()->session['zip']:'' ?>"/>
                                            <span class="help-block"> Provide your zip code </span>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label col-md-3">City/Town
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="city" value="<?php echo isset(Yii::app()->session['city'])?Yii::app()->session['city']:'' ?>"/>
                                            <span class="help-block"> Provide your city or town </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab3">
                                    <h3 class="block">Provide your payment details</h3>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Account owner
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="ac_owner"  />
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">IBAN
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="iban" />
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab4">
                                    <div class="" id="loading_note">
                                        <div class="note note-warning" >
                                            <h4 class="block">Processing...</h4>
                                            <p> Please wait ! </p>
                                        </div>
                                    </div>
                                    <div class="hidden" id="success_note">
                                        <div class="note note-success " >
                                            <h4 class="block">Congrats! Welcome in the Wunder family</h4>
                                            <p> We are happy to announce that your account has been created.
                                                We welcome you to Wunder.

                                                If you have any questions, do not hesitate to contact us via the support. </p>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Payment Id:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" id="payment_id"> </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="hidden" id="error_note">
                                        <div class="note note-danger " >
                                            <h4 class="block">Error! Failed to create your account</h4>
                                            <p> Please refresh this page and try again !

                                                If the problem persists, please contact support. </p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <a href="javascript:;" class="btn default button-previous">
                                        <i class="fa fa-angle-left"></i> Back </a>
                                    <a href="javascript:;"  id="btn_next" class="btn btn-outline green button-next"> Continue
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                    <a href="javascript:;" class="btn green button-submit"> Submit
                                        <i class="fa fa-check"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>